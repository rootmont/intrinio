GUIDE INSTALLATION SERVER

Installing python

- apt install python3.6
- apt install python3-pip

Creating virtual env

- python3.6 -m pip install virtualenv
- virtualenv venv

Activar virtualenv

- source venv/bin/activate

Instalar paquetes python

- python3.6 -m pip install -r requirements.txt

Defining environment variables

- export env=database (in server or localdb in local)

Run project using:

- python3.6 flask_app.py

See server in 

- Running on http://127.0.0.1:5000/

For deployment use:

- zappa deploy or zappa update
