cd ${HOME}/stocks/api-intrinio-ms
echo "Starting intrinio-get-stock-prices-by-security-adjustments-bulk thread"
source venv/bin/activate
export FLASK_APP=flask_app.py
export env=database
python3.6 -m flask intrinio-get-stock-prices-by-security-adjustments-bulk