
from intrinio.flask_factory import app
from intrinio.router import configure_api

configure_api(app)


if __name__ == '__main__':
    app.run()
