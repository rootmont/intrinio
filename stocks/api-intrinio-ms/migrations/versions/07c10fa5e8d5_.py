"""empty message

Revision ID: 07c10fa5e8d5
Revises: b9078c8c4c18
Create Date: 2019-02-21 11:21:12.360366

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '07c10fa5e8d5'
down_revision = 'b9078c8c4c18'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('us_insider_ownership', sa.Column('ticker', sa.String(length=191, collation='utf8mb4_unicode_ci'), nullable=True))
    op.create_index(op.f('ix_us_insider_ownership_ticker'), 'us_insider_ownership', ['ticker'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_us_insider_ownership_ticker'), table_name='us_insider_ownership')
    op.drop_column('us_insider_ownership', 'ticker')
    # ### end Alembic commands ###
