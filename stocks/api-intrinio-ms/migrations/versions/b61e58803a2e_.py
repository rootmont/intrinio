"""empty message

Revision ID: b61e58803a2e
Revises: bd1846c80778
Create Date: 2019-02-09 11:48:50.716192

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b61e58803a2e'
down_revision = 'bd1846c80778'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table('security_stock_prices_adjustments',
    sa.Column('created_at', sa.DateTime(), nullable=True),
    sa.Column('updated_at', sa.DateTime(), nullable=True),
    sa.Column('deleted_at', sa.DateTime(), nullable=True),
    sa.Column('id', sa.Integer(), nullable=False),
    sa.Column('date', sa.DateTime(), nullable=True),
    sa.Column('dividend_currency', sa.String(length=191, collation='utf8mb4_unicode_ci'), nullable=True),
    sa.Column('split_ratio', sa.Float(precision='20, 10'), nullable=True),
    sa.Column('factor', sa.Float(precision='20, 10'), nullable=True),
    sa.Column('dividend', sa.Float(precision='20, 10'), nullable=True),
    sa.Column('identifier', sa.String(length=191, collation='utf8mb4_unicode_ci'), nullable=True),
    sa.Column('security_id', sa.String(length=191, collation='utf8mb4_unicode_ci'), nullable=True),
    sa.Column('company_id', sa.String(length=191, collation='utf8mb4_unicode_ci'), nullable=True),
    sa.Column('name', sa.String(length=191, collation='utf8mb4_unicode_ci'), nullable=True),
    sa.Column('code', sa.String(length=191, collation='utf8mb4_unicode_ci'), nullable=True),
    sa.Column('currency', sa.String(length=191, collation='utf8mb4_unicode_ci'), nullable=True),
    sa.Column('ticker', sa.String(length=191, collation='utf8mb4_unicode_ci'), nullable=True),
    sa.Column('composite_ticker', sa.String(length=191, collation='utf8mb4_unicode_ci'), nullable=True),
    sa.Column('figi', sa.String(length=191, collation='utf8mb4_unicode_ci'), nullable=True),
    sa.Column('composite_figi', sa.String(length=191, collation='utf8mb4_unicode_ci'), nullable=True),
    sa.Column('share_class_figi', sa.String(length=191, collation='utf8mb4_unicode_ci'), nullable=True),
    sa.Column('next_page', sa.String(length=191, collation='utf8mb4_unicode_ci'), nullable=True),
    sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_security_stock_prices_adjustments_code'), 'security_stock_prices_adjustments', ['code'], unique=False)
    op.create_index(op.f('ix_security_stock_prices_adjustments_company_id'), 'security_stock_prices_adjustments', ['company_id'], unique=False)
    op.create_index(op.f('ix_security_stock_prices_adjustments_composite_figi'), 'security_stock_prices_adjustments', ['composite_figi'], unique=False)
    op.create_index(op.f('ix_security_stock_prices_adjustments_composite_ticker'), 'security_stock_prices_adjustments', ['composite_ticker'], unique=False)
    op.create_index(op.f('ix_security_stock_prices_adjustments_currency'), 'security_stock_prices_adjustments', ['currency'], unique=False)
    op.create_index(op.f('ix_security_stock_prices_adjustments_figi'), 'security_stock_prices_adjustments', ['figi'], unique=False)
    op.create_index(op.f('ix_security_stock_prices_adjustments_identifier'), 'security_stock_prices_adjustments', ['identifier'], unique=False)
    op.create_index(op.f('ix_security_stock_prices_adjustments_name'), 'security_stock_prices_adjustments', ['name'], unique=False)
    op.create_index(op.f('ix_security_stock_prices_adjustments_security_id'), 'security_stock_prices_adjustments', ['security_id'], unique=False)
    op.create_index(op.f('ix_security_stock_prices_adjustments_share_class_figi'), 'security_stock_prices_adjustments', ['share_class_figi'], unique=False)
    op.create_index(op.f('ix_security_stock_prices_adjustments_ticker'), 'security_stock_prices_adjustments', ['ticker'], unique=False)
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_index(op.f('ix_security_stock_prices_adjustments_ticker'), table_name='security_stock_prices_adjustments')
    op.drop_index(op.f('ix_security_stock_prices_adjustments_share_class_figi'), table_name='security_stock_prices_adjustments')
    op.drop_index(op.f('ix_security_stock_prices_adjustments_security_id'), table_name='security_stock_prices_adjustments')
    op.drop_index(op.f('ix_security_stock_prices_adjustments_name'), table_name='security_stock_prices_adjustments')
    op.drop_index(op.f('ix_security_stock_prices_adjustments_identifier'), table_name='security_stock_prices_adjustments')
    op.drop_index(op.f('ix_security_stock_prices_adjustments_figi'), table_name='security_stock_prices_adjustments')
    op.drop_index(op.f('ix_security_stock_prices_adjustments_currency'), table_name='security_stock_prices_adjustments')
    op.drop_index(op.f('ix_security_stock_prices_adjustments_composite_ticker'), table_name='security_stock_prices_adjustments')
    op.drop_index(op.f('ix_security_stock_prices_adjustments_composite_figi'), table_name='security_stock_prices_adjustments')
    op.drop_index(op.f('ix_security_stock_prices_adjustments_company_id'), table_name='security_stock_prices_adjustments')
    op.drop_index(op.f('ix_security_stock_prices_adjustments_code'), table_name='security_stock_prices_adjustments')
    op.drop_table('security_stock_prices_adjustments')
    # ### end Alembic commands ###
