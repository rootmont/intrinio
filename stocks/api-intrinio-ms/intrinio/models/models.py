from intrinio.flask_factory import db

from sqlalchemy import Index

from sqlalchemy.schema import FetchedValue

# coding: utf-8
from sqlalchemy import Column, DateTime, ForeignKey, Integer, Numeric, SmallInteger, String, Text
from sqlalchemy.dialects.mysql import INTEGER, TINYINT, LONGTEXT, SMALLINT, TIMESTAMP, BIGINT

from sqlalchemy.schema import FetchedValue
from sqlalchemy.orm import relationship
from sqlalchemy.ext.declarative import declarative_base

from intrinio.models.mixins import TimeStampsMixin, SoftDeletesMixin

from datetime import datetime, timedelta

Base = declarative_base()
metadata = Base.metadata


class ApiResponseSecurityStockPrices(db.Model, TimeStampsMixin, SoftDeletesMixin):
    __tablename__ = 'security_stock_prices'


    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime, nullable=True, index=True)
    intraperiod = db.Column(db.Boolean, nullable=True, index=True)
    frequency = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    open = db.Column(db.Float('20, 10'), nullable=True )
    high = db.Column(db.Float('20, 5'), nullable=True )
    low = db.Column(db.Float('20, 5'), nullable=True )
    close = db.Column(db.Float('20, 5'), nullable=True )
    volume = db.Column(db.Float('20, 5'), nullable=True )
    adj_open = db.Column(db.Float('20, 5'), nullable=True )
    adj_high = db.Column(db.Float('20, 5'), nullable=True )
    adj_low = db.Column(db.Float('20, 5'), nullable=True )
    adj_close = db.Column(db.Float('20, 5'), nullable=True )
    adj_volume = db.Column(db.Float('20, 5'), nullable=True )

    identifier = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    security_id = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    company_id = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    name = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    code = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    currency = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    ticker = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    composite_ticker = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    figi = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    composite_figi = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    share_class_figi = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)

    next_page = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)


class ApiResponseCompaniesList(db.Model, TimeStampsMixin, SoftDeletesMixin):
    __tablename__ = 'companies_list'


    id = db.Column(db.Integer, primary_key=True)
    ticker = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=False, index=True)

    complete_1 =  db.Column(db.Boolean, nullable=True, default=False)
    error_1 =  db.Column(db.Boolean, nullable=True, default=False)
    progress_1 =  db.Column(db.Boolean, nullable=True, default=False)

    complete_2 =  db.Column(db.Boolean, nullable=True, default=False)
    error_2 =  db.Column(db.Boolean, nullable=True, default=False)
    progress_2 =  db.Column(db.Boolean, nullable=True, default=False)
       
    complete_3 =  db.Column(db.Boolean, nullable=True, default=False)
    error_3 =  db.Column(db.Boolean, nullable=True, default=False)
    progress_3 =  db.Column(db.Boolean, nullable=True, default=False)

    complete_4 =  db.Column(db.Boolean, nullable=True, default=False)
    error_4 =  db.Column(db.Boolean, nullable=True, default=False)
    progress_4 =  db.Column(db.Boolean, nullable=True, default=False)

    complete_5 =  db.Column(db.Boolean, nullable=True, default=False)
    error_5 =  db.Column(db.Boolean, nullable=True, default=False)
    progress_5 =  db.Column(db.Boolean, nullable=True, default=False)

    complete_6 =  db.Column(db.Boolean, nullable=True, default=False)
    error_6 =  db.Column(db.Boolean, nullable=True, default=False)
    progress_6 =  db.Column(db.Boolean, nullable=True, default=False)
      
    complete_7 =  db.Column(db.Boolean, nullable=True, default=False)
    error_7 =  db.Column(db.Boolean, nullable=True, default=False)
    progress_7 =  db.Column(db.Boolean, nullable=True, default=False)

    complete_8 =  db.Column(db.Boolean, nullable=True, default=False)
    error_8 =  db.Column(db.Boolean, nullable=True, default=False)
    progress_8 =  db.Column(db.Boolean, nullable=True, default=False)

    complete_9 =  db.Column(db.Boolean, nullable=True, default=False)
    error_9 =  db.Column(db.Boolean, nullable=True, default=False)
    progress_9 =  db.Column(db.Boolean, nullable=True, default=False)

    complete_10 =  db.Column(db.Boolean, nullable=True, default=False)
    error_10 =  db.Column(db.Boolean, nullable=True, default=False)
    progress_10 =  db.Column(db.Boolean, nullable=True, default=False)
      
    complete_11 =  db.Column(db.Boolean, nullable=True, default=False)
    error_11 =  db.Column(db.Boolean, nullable=True, default=False)
    progress_11 =  db.Column(db.Boolean, nullable=True, default=False)

    complete_12 =  db.Column(db.Boolean, nullable=True, default=False)
    error_12 =  db.Column(db.Boolean, nullable=True, default=False)
    progress_12 =  db.Column(db.Boolean, nullable=True, default=False)

    complete_13 =  db.Column(db.Boolean, nullable=True, default=False)
    error_13 =  db.Column(db.Boolean, nullable=True, default=False)
    progress_13 =  db.Column(db.Boolean, nullable=True, default=False)
  

class ApiResponseSecurityStockPricesAdjustments(db.Model, TimeStampsMixin, SoftDeletesMixin):
    __tablename__ = 'security_stock_prices_adjustments'

    id = db.Column(db.Integer, primary_key=True)
    date = db.Column(db.DateTime, nullable=True ,index=True)
    dividend_currency = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True)
    split_ratio = db.Column(db.Float('20, 10'), nullable=True )
    factor = db.Column(db.Float('20, 10'), nullable=True )
    dividend = db.Column(db.Float('20, 10'), nullable=True )

    identifier = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    security_id = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    company_id = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    name = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    code = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    currency = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    ticker = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    composite_ticker = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    figi = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    composite_figi = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    share_class_figi = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)

    next_page = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True)

class ApiResponseCompanyFundamentals(db.Model, TimeStampsMixin, SoftDeletesMixin):
    __tablename__ = 'company_fundamentals'

    id = db.Column(db.Integer, primary_key=True)

    fundamental_id = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    statement_code = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    fiscal_year = db.Column(db.Float('20, 10'), nullable=True , index=True)
    fiscal_period = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    type = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    start_date = db.Column(db.DateTime, nullable=True, index=True)
    end_date = db.Column(db.DateTime, nullable=True, index=True)
    filing_date = db.Column(db.DateTime, nullable=True, index=True)

    
    intrinio_id = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    ticker = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    name = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    lei = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    cik = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)

    next_page = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)


class ApiResponseStandardizedFinancials(db.Model, TimeStampsMixin, SoftDeletesMixin):
    __tablename__ = 'fundamental_standardized_financials'

    id = db.Column(db.Integer, primary_key=True)
    
    company =  db.Column(db.Boolean, nullable=True)
    intrinio_id = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    ticker = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    name = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    lei = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    cik = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)

    end_date = db.Column(db.DateTime, nullable=True, index=True)
    filing_date = db.Column(db.DateTime, nullable=True, index=True)
    fiscal_period = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    fiscal_year = db.Column(db.Float('20, 10'), nullable=True , index=True)
    fundamental_id = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    start_date = db.Column(db.DateTime, nullable=True, index=True)
    statement_code = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    type = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)

    data_tag = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)    
    tag_id = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    tag_name = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    tag = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    unit = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    value = db.Column(db.Float('25, 10'), nullable=True , index=True)

    next_page = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)


class ApiResponseCompanyNews(db.Model, TimeStampsMixin, SoftDeletesMixin):
    __tablename__ = 'company_news'


    id = db.Column(db.Integer, primary_key=True)
    
    intrinio_id = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    ticker = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    name = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    lei = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    cik = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)


    news_id = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    publication_date = db.Column(db.DateTime, nullable=True, index=True)
    summary = db.Column(LONGTEXT(collation='utf8mb4_unicode_ci'), nullable=False)
    title = db.Column(LONGTEXT(collation='utf8mb4_unicode_ci'), nullable=False)
    url = db.Column(LONGTEXT(collation='utf8mb4_unicode_ci'), nullable=False)

    next_page = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)

class ApiResponseCompany(db.Model, TimeStampsMixin, SoftDeletesMixin):
    __tablename__ = 'company_information'

    id = db.Column(db.Integer, primary_key=True)

    intrinio_id = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    ticker = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    name = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    lei = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    cik = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)

    legal_name = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    stock_exchange = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    sic  = db.Column(db.Float('20, 10'), nullable=True , index=True)
    short_description = db.Column(LONGTEXT(collation='utf8mb4_unicode_ci'), nullable=True)
    ceo = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True)
    company_url = db.Column(LONGTEXT(collation='utf8mb4_unicode_ci'), nullable=True)
    business_address = db.Column(LONGTEXT(collation='utf8mb4_unicode_ci'), nullable=True)
    mailing_address = db.Column(LONGTEXT(collation='utf8mb4_unicode_ci'), nullable=True)
    business_phone_no = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True)
    hq_address1 = db.Column(LONGTEXT(collation='utf8mb4_unicode_ci'), nullable=True)
    hq_address2 = db.Column(LONGTEXT(collation='utf8mb4_unicode_ci'), nullable=True)
    hq_address_city = db.Column(LONGTEXT(collation='utf8mb4_unicode_ci'), nullable=True)
    hq_address_postal_code = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True)
    entity_legal_form = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True)

    latest_filing_date = db.Column(db.DateTime, nullable=True, index=True)
    hq_state = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    hq_country = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    inc_state = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    inc_country = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    employees  = db.Column(db.Float('20, 10'), nullable=True , index=True)
    entity_status = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    sector = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    industry_category = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    industry_group = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    template = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    standardized_active =  db.Column(db.Boolean, nullable=True , index=True)


class ApiResponseInsiderTransactions(db.Model, TimeStampsMixin, SoftDeletesMixin):
    __tablename__ = 'us_insider_transactions'

    id = db.Column(db.Integer, primary_key=True)

    ticker = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    owner_cik =   db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    owner_name =   db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    filing_date =  db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    filing_url =  db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    director =  db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True)
    officer = db.Column(db.Boolean, nullable=True)
    ten_percent_owner =  db.Column(db.Boolean, nullable=True)
    other_relation =  db.Column(db.Boolean, nullable=True)
    officer_title =   db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True)
    derivative_transaction =  db.Column(db.Boolean, nullable=True)
    security_title =   db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True)
    conversion_exercise_price = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True)
    transaction_date =   db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True)
    deemed_execution_date =  db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True)
    transaction_type_code =   db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True)
    acquisition_disposition_code =   db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True)
    amount_of_shares =  db.Column(db.Float('20, 10'), nullable=True)
    exercise_date  = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True)
    expiration_date  = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True)
    underlying_security_title  =  db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True)
    underlying_shares  = db.Column(db.Float('20, 10'), nullable=True , index=True)
    transaction_price =  db.Column(db.Float('20, 10'), nullable=True , index=True)
    total_shares_owned =  db.Column(db.Float('20, 10'), nullable=True)
    ownership_type_code =   db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    report_line_number =  db.Column(db.Float('20, 10'), nullable=True)
    result_count = db.Column(INTEGER, nullable=True)
    page_size = db.Column(INTEGER, nullable=True)
    current_page = db.Column(INTEGER, nullable=True)
    total_pages = db.Column(INTEGER, nullable=True)
    api_call_credits = db.Column(INTEGER, nullable=True)


class ApiResponseInsiderOwnerShip(db.Model, TimeStampsMixin, SoftDeletesMixin):
    __tablename__ = 'us_insider_ownership'

    id = db.Column(db.Integer, primary_key=True)

    ticker = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    owner_cik =   db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    owner_name =   db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    last_reported_date =   db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    value =  db.Column(db.Float('20, 10'), nullable=True , index=True)
    amount =  db.Column(db.Float('20, 10'), nullable=True , index=True)
    result_count = db.Column(INTEGER, nullable=True)
    page_size = db.Column(INTEGER, nullable=True)
    current_page = db.Column(INTEGER, nullable=True)
    total_pages = db.Column(INTEGER, nullable=True)
    api_call_credits = db.Column(INTEGER, nullable=True)

    
class ApiResponseOwners(db.Model, TimeStampsMixin, SoftDeletesMixin):
    __tablename__ = 'owners'

    id = db.Column(db.Integer, primary_key=True)
    
    ticker = db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True, index=True)
    owner_cik =   db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    owner_name =   db.Column(db.String(191, 'utf8mb4_unicode_ci'), nullable=True , index=True)
    result_count = db.Column(INTEGER, nullable=True)
    page_size = db.Column(INTEGER, nullable=True)
    current_page = db.Column(INTEGER, nullable=True)
    total_pages = db.Column(INTEGER, nullable=True)
    api_call_credits = db.Column(INTEGER, nullable=True)



