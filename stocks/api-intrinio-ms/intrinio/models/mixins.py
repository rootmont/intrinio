import sqlalchemy

from datetime import datetime


class TimeStampsMixin(object):
    created_at = sqlalchemy.Column(sqlalchemy.DateTime, default=lambda: datetime.utcnow())
    updated_at = sqlalchemy.Column(sqlalchemy.DateTime, default=lambda: datetime.utcnow(), onupdate=lambda: datetime.utcnow())


class SoftDeletesMixin(object):
    deleted_at = sqlalchemy.Column(sqlalchemy.DateTime, nullable=True)
