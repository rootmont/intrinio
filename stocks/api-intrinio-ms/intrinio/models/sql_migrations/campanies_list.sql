CREATE TABLE `dev`.`companies_list` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `ticker` VARCHAR(191) NULL,
  `created_at` datetime,
  `deleted_at` datetime,
  `updated_at` datetime,
  PRIMARY KEY (`id`));
