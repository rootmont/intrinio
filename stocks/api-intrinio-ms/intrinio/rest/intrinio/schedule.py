# coding: utf-8

import logging
import json
import time
import datetime
from zappa.async import task


from flask_restplus import Resource
from intrinio.app.intrinio import ( 
    intrinioModuleStockPricesBySecurity, 
    intrinioModuleCompaniesList, 
    intrinioModuleRequesterSystem, 
    StockPricesBySecurityAdjustmentsRS,
    intrinioModuleCompaniesFundamentals,
    FundamentalStandarizedFinancialsRS,
    CompanyNewsRS,
    CompanyRS, 
    SecurityStocksPricesBulkRS,
    SecurityStocksPricesAdjustmentsBulkRS,
    InsiderOwnerShipRS,
    OwnersRS,
    InsiderTransactionsRS)

#SYNC Methods

@task
def intrinio_get_companies_list():
    intrinio_instance = intrinioModuleCompaniesList()
    return intrinio_instance.request()
@task
def stock_prices_by_security():
    intrinio_instance = intrinioModuleRequesterSystem()
    return intrinio_instance.request()
@task
def stock_prices_by_security_adjustments():
    intrinio_instance = StockPricesBySecurityAdjustmentsRS()
    return intrinio_instance.request()
@task
def intrinio_get_company_fundamentals():
    intrinio_instance = intrinioModuleCompaniesFundamentals()
    return intrinio_instance.request()
@task
def intrinio_get_fundamentals_information():
    intrinio_instance = FundamentalStandarizedFinancialsRS()
    return intrinio_instance.request()
@task
def intrinio_get_company_news():
    intrinio_instance = CompanyNewsRS()
    return intrinio_instance.request()
@task
def intrinio_get_company_information():
    intrinio_instance = CompanyRS()
    return intrinio_instance.request()
@task
def intrinio_get_stock_prices_by_security_bulk():
    intrinio_instance = SecurityStocksPricesBulkRS()
    return intrinio_instance.request()
@task
def intrinio_get_stock_prices_by_security_adjustments_bulk():
    intrinio_instance = SecurityStocksPricesAdjustmentsBulkRS()
    return intrinio_instance.request()

@task
def intrinio_get_insider_owner_ship():
    intrinio_instance = InsiderOwnerShipRS()
    return intrinio_instance.request()

@task
def intrinio_get_owners():
    intrinio_instance = OwnersRS()
    return intrinio_instance.request()

@task
def intrinio_get_insider_transactions():
    intrinio_instance = InsiderTransactionsRS()
    return intrinio_instance.request()

    