import json
from flask_restplus import fields
from intrinio.router import api


stock_prices_by_security = api.model('intrinio_stock_prices_by_security', {
    'identifier': fields.String(required=True, description='identifier'),
    'start_date': fields.String(required=True, description='start_date'),
    'end_date': fields.String(required=True, description='end_date'),
    'frequency': fields.String(required=True, description='frequency'),
    'next_page': fields.String(required=True, description='next_page') 
})


stock_prices_by_security_adjustments = api.model('intrinio_stock_prices_by_security_adjustments', {
    'identifier': fields.String(required=True, description='identifier'),
    'start_date': fields.String(required=True, description='start_date'),
    'end_date': fields.String(required=True, description='end_date'),
    'next_page': fields.String(required=True, description='next_page') 
})

companies_list = api.model('intrinio_companies_list', {
    'next_page': fields.String(required=False, description='next_page') 
})