# coding: utf-8

import logging
import json
import time
import datetime

from zappa.async import task

from flask_restplus import Resource
from intrinio.rest.api import api
from intrinio.rest.intrinio.schedule import *

from intrinio.rest.intrinio.serializers import (
    stock_prices_by_security, 
    companies_list, 
    stock_prices_by_security_adjustments)

from intrinio.app.intrinio import (
    intrinioModuleStockPricesBySecurity, 
    intrinioModuleCompaniesList, 
    intrinioModuleStockPricesBySecurityAdjustments)

log = logging.getLogger(__name__)


ns = api.namespace('intrinio', description='intrinio service')

@ns.route('/stock_prices_by_security')
class IntrinioService(Resource):

    @api.expect(stock_prices_by_security, validate=True)
    def post(self):
        intrinio_instance = intrinioModuleStockPricesBySecurity()
        return intrinio_instance.request(**api.payload)

@ns.route('/companies_list')
class IntrinioServiceCompaniesList(Resource):

    def get(self):
        intrinio_instance = intrinioModuleCompaniesList()
        return intrinio_instance.request()


@ns.route('/security_stock_prices_adjustments')
class IntrinioServiceSecurityStockPricesAdjustments(Resource):

    @api.expect(stock_prices_by_security_adjustments, validate=True)
    def post(self):
        intrinio_instance = intrinioModuleStockPricesBySecurityAdjustments()
        return intrinio_instance.request(**api.payload)
