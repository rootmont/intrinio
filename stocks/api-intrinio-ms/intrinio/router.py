import traceback
import logging
from intrinio.flask_factory import settings
from flask import Blueprint
from flask_cors import CORS
from intrinio.rest.api import api
from intrinio.rest.intrinio.api import ns as intrinio_namespace


log = logging.getLogger(__name__)


def _configure_namespaces(api):
	"""
		Add more namespaces HERE
	"""
	#intrinio_namespace
	api.add_namespace(intrinio_namespace)


def configure_api(flask_app):
	blueprint = Blueprint('api', __name__)
	api.init_app(blueprint)
	_configure_namespaces(api)
	CORS(flask_app)
	flask_app.register_blueprint(blueprint)
