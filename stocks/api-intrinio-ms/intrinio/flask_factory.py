from flask import Flask
from flask_dotenv import DotEnv
from flask_migrate import Migrate
from sqlalchemy import create_engine
from flask_sqlalchemy import SQLAlchemy
import pymysql
pymysql.install_as_MySQLdb()

from intrinio.app.config import connection_string

import configparser

import click

app = Flask(__name__)

env = DotEnv()
env.init_app(app)

app.config['SQLALCHEMY_DATABASE_URI'] = connection_string
app.config['SECRET_KEY'] = 'supersecret'
app.config['SQLALCHEMY_COMMIT_ON_TEARDOWN'] = True
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SQLALCHEMY_POOL_SIZE'] = 10
app.config['SQLALCHEMY_POOL_TIMEOUT'] = 30
app.config['SQLALCHEMY_POOL_RECYCLE'] = 60
app.config['SQLALCHEMY_MAX_OVERFLOW'] = 5

settings  = app.config

db = SQLAlchemy(app)

migrate = Migrate(app, db)


@app.cli.command()
def stock_prices_by_security():

    from intrinio.rest.intrinio.schedule import stock_prices_by_security

    print("Initializing command for stock_prices_by_security")

    stock_prices_by_security()

    return True


@app.cli.command()
def stock_prices_by_security_adjustments():

    from intrinio.rest.intrinio.schedule import stock_prices_by_security_adjustments

    print("Initializing command for stock_prices_by_security_adjustments")

    stock_prices_by_security_adjustments()

    return True


@app.cli.command()
def intrinio_get_company_fundamentals():

    from intrinio.rest.intrinio.schedule import intrinio_get_company_fundamentals

    print("Initializing command for intrinio_get_company_fundamentals")

    intrinio_get_company_fundamentals()

    return True


@app.cli.command()
def intrinio_get_fundamentals_information():

    from intrinio.rest.intrinio.schedule import intrinio_get_fundamentals_information

    print("Initializing command for intrinio_get_fundamentals_information")

    intrinio_get_fundamentals_information()

    return True


@app.cli.command()
def intrinio_get_company_news():

    from intrinio.rest.intrinio.schedule import intrinio_get_company_news

    print("Initializing command for intrinio_get_company_news")

    intrinio_get_company_news()

    return True


@app.cli.command()
def intrinio_get_company_information():

    from intrinio.rest.intrinio.schedule import intrinio_get_company_information

    print("Initializing command for intrinio_get_company_information")

    intrinio_get_company_information()

    return True


@app.cli.command()
def intrinio_get_stock_prices_by_security_bulk():

    from intrinio.rest.intrinio.schedule import intrinio_get_stock_prices_by_security_bulk

    print("Initializing command for intrinio_get_stock_prices_by_security_bulk")

    intrinio_get_stock_prices_by_security_bulk()

    return True


@app.cli.command()
def intrinio_get_stock_prices_by_security_adjustments_bulk():

    from intrinio.rest.intrinio.schedule import intrinio_get_stock_prices_by_security_adjustments_bulk

    print("Initializing command for intrinio_get_stock_prices_by_security_adjustments_bulk")

    intrinio_get_stock_prices_by_security_adjustments_bulk()

    return True


@app.cli.command()
def intrinio_get_insider_owner_ship():

    from intrinio.rest.intrinio.schedule import intrinio_get_insider_owner_ship

    print("Initializing command for intrinio_get_insider_owner_ship")

    intrinio_get_insider_owner_ship()

    return True


@app.cli.command()
def intrinio_get_owners():

    from intrinio.rest.intrinio.schedule import intrinio_get_owners

    print("Initializing command for intrinio_get_owners")

    intrinio_get_owners()

    return True


@app.cli.command()
def intrinio_get_insider_transactions():

    from intrinio.rest.intrinio.schedule import intrinio_get_insider_transactions

    print("Initializing command for intrinio_get_insider_transactions")

    intrinio_get_insider_transactions()

    return True
