import datetime 
from datetime import datetime as dat

def json_datetime_date_convertor(o):

    if isinstance(o, datetime.datetime):

        return o.__str__()

    elif isinstance(o, datetime.date):

        return dat.utcfromtimestamp(int(o.strftime('%s')))


def to_str(obj):
    return str(obj)

def datetime_to_str(obj):
    return obj.isoformat().replace('T', ' ')


def datetime_to_timestamp(obj):
    import pytz
    if obj is not None:
        return obj.replace(tzinfo=pytz.UTC).timestamp() * 1000

    return obj


def datetime_to_timestamp_seconds(obj):
    import pytz
    if obj is not None:
        return obj.replace(tzinfo=pytz.UTC).timestamp()

    return obj


def date_to_str(obj):
    return obj.isoformat()


def str_to_json(obj):
    import json
    return json.loads(obj)


def str_to_dict(obj):
    from ast import literal_eval
    return literal_eval(obj)


def bytes_to_str(obj):
    if isinstance(obj, bytes):
        return obj.decode('utf-8')

    return obj


def timedelta_to_float_seconds(obj):
    from datetime import timedelta

    if isinstance(obj, timedelta):
        return obj.total_seconds()

    return obj



def to_dict(inst, timestamp=True):
    """
    Jsonify the sql alchemy model
    """
    from decimal import Decimal
    from datetime import date, datetime, timedelta

    if inst is None:
        return inst

    cls = type(inst)
    convert = dict({
        Decimal: to_str,
        datetime: datetime_to_str,
        date: date_to_str,
        bytes: bytes_to_str,
        timedelta: timedelta_to_float_seconds
    })

    def check_instance(obj):
        for key in convert.keys():
            if isinstance(obj, key):
                return True
        return False

    d = dict()
    
    for c in cls.__table__.columns:

        try:
            v = getattr(inst, c.name)

            if (c.type in convert.keys() or c.name in names_to_convert.keys() or c.name in getattr(cls, 'names_to_convert', dict()).keys() or check_instance(v)) and v is not None:
                tmp_dict = dict(convert, **names_to_convert, **getattr(cls, 'names_to_convert', dict()))

                try:
                    d[c.name] = tmp_dict[v.__class__ if v.__class__ in tmp_dict else c.name](v)

                    if isinstance(v, datetime) and timestamp:
                        d["{}_timestamp".format(c.name)] = datetime_to_timestamp(v)
                        d["{}_timestamp_seconds".format(c.name)] = datetime_to_timestamp_seconds(v)

                except:
                    d[c.name] = "Error: Failed to convert using ", str(type(v))

            elif v in [None, '']:
                d[c.name] = None

            else:
                d[c.name] = v

        except:
            pass
            # No parse was posible, actually working on a new functionality

    return d




class Timer:
    """
        Tracker using time
    """
    def __init__(self):
        self.start = dat.now()
 
    def log(self, desc=None):
        difference = dat.now() - self.start
        if(desc):
            rootLogger.info("** {} - {}".format(difference, desc))
        else:
            rootLogger.info("** {}".format(difference))
 
    def stop(self):
        self.log(desc="Finished")
        self.start = dat.now()
 
    def reset_timer(self):
        self.start = dat.now()