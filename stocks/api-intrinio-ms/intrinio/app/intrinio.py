from __future__ import print_function
import random
import subprocess
import os
import datetime
from datetime import datetime as dat
import json
import requests
import time
from pprint import pprint
import logging
import random
from ast import literal_eval

import intrinio_sdk
from intrinio_sdk.rest import ApiException
from intrinio.app.utils import json_datetime_date_convertor, Timer
from intrinio.app.config import API_KEY
from intrinio.models.models import (
    ApiResponseSecurityStockPrices, 
    ApiResponseCompaniesList, 
    ApiResponseSecurityStockPricesAdjustments,
    ApiResponseCompanyFundamentals,
    ApiResponseStandardizedFinancials,
    ApiResponseCompanyNews,
    ApiResponseCompany,
    ApiResponseInsiderOwnerShip,
    ApiResponseInsiderTransactions,
    ApiResponseOwners)

from intrinio.flask_factory import db

from zappa.async import task

logFormatter = logging.Formatter('%(asctime)s - %(name)s - [%(threadName)-12.12s] - %(levelname)s - %(message)s')
rootLogger = logging.getLogger()

fileHandler = logging.FileHandler("/tmp/{0}.log".format("intrinio"))
fileHandler.setFormatter(logFormatter)
rootLogger.addHandler(fileHandler)

consoleHandler = logging.StreamHandler()
consoleHandler.setFormatter(logFormatter)
rootLogger.addHandler(consoleHandler)
rootLogger.setLevel("INFO")

############################################ VARIABLES ###############################################

sleep_time = 2

############################################ COMPANIES LIST API ###############################################


class intrinioModuleCompaniesList():

    def company_object_iterator(self, api_response):
        
        if api_response is not None:

            list_tickers = []

            added_counter = 0
            for company in api_response.companies:

                preview_company = ApiResponseCompaniesList.query.filter_by(ticker=company.ticker).all()
                
                if preview_company:

                    continue
                
                if company.ticker is None:

                    continue

                company_new = ApiResponseCompaniesList(ticker=company.ticker)

                db.session.merge(company_new)

                added_counter = added_counter + 1
            
                list_tickers.append(company.ticker)

            db.session.commit()
                        
            return list_tickers

        else:

            rootLogger.info("no object which iterate!")
            return []


    def companies_crawler(self, next_page=None, first_page=True, tickers=[]):
                
        rootLogger.info("Step 1: companies_crawler loop, current ticker size: {}".format(len(tickers)))

        intrinio_sdk.ApiClient().configuration.api_key['api_key'] =  'OWZlYzUyNzYwODg1MWNhNjZmZTZjZmE5ZjgxMTZmMjg6YTVhOTRhNjM5MmE2OWJmOTY3NGUzOTJiMjlmZmQwNzk'

        company_api = intrinio_sdk.CompanyApi()

        if next_page is None and first_page is False:

            rootLogger.info("last page")

            return tickers
            
        elif next_page is None and first_page:

            rootLogger.info("first page")

            try:

                rootLogger.info("companies_crawler 0.0")

                api_response = company_api.get_all_companies()

                tickers = tickers + self.company_object_iterator(api_response)

                self.companies_crawler(next_page=api_response.next_page, first_page=False, tickers=tickers)

            except ApiException as e:

                time.sleep(sleep_time)

                rootLogger.info("Exception when calling CompanyApi->get_all_companies: %s\n" % e)

        else:

            try:
                
                rootLogger.info("companies_crawler 1.0")

                api_response = company_api.get_all_companies(next_page=next_page)

                rootLogger.info("companies_crawler 1.1")

                tickers = tickers + self.company_object_iterator(api_response)
               
                rootLogger.info("companies_crawler 2.0")

                self.companies_crawler(next_page=api_response.next_page, first_page=False, tickers=tickers)

            except ApiException as e:

                time.sleep(sleep_time)

                rootLogger.info("Exception when calling CompanyApi->get_all_companies: %s\n" % e)

            rootLogger.info(api_response.next_page)

            rootLogger.info(api_response.companies[0].ticker)
    
    def request(self, next_page=None):

        response_time = Timer()

        # next_page = '' # str | Gets the next page of data from a previous API call (optional)

        payload = {"result":True}

        if isinstance(next_page, str) or next_page is None:
            pass
        else: 
            payload["status"] = "error"
            payload["reason"] = "next_page is not a valid format or string format"

        rootLogger.info("API is starting 1.0")

        try:

            response = self.companies_crawler()    
            
            payload["companies_tickers"] =  response
            #payload["total_companies"] =  len(response)
            
            rootLogger.info("API is starting 2.0")

        except Exception as e:

            rootLogger.info("Exception when calling CompanyApi->get_all_companies: %s\n" % e)

            payload = {"result":False}

            payload["reason"] = "API-Exception, please check system logs"

            time.sleep(sleep_time)

            return payload
                
        return payload



############################################ FIRST API ###############################################


class intrinioModuleStockPricesBySecurity():
    
    def request(self, identifier, start_date, end_date, frequency, next_page):

        response_time = Timer()

        rootLogger.info("API is starting")
        # identifier = 'AAPL' # str | A Security identifier (Ticker, FIGI, ISIN, CUSIP, Intrinio ID)
        # start_date = '2018-01-01' # date | Return prices on or after the date (optional)
        # end_date = '2019-01-01' # date | Return prices on or before the date (optional)
        # frequency = 'daily' # str | Return stock prices in the given frequency (optional) (default to daily)
        # next_page = '' # str | Gets the next page of data from a previous API call (optional)

        payload = {"result":True}

        if isinstance(identifier, str) and identifier is not None:
            pass
        else: 
            payload["status"] = "error"
            payload["reason"] = "identifier is not a valid format or string format"

        if isinstance(start_date, str) and start_date is not None:
            pass
        else: 
            payload["status"] = "error"
            payload["reason"] = "start_date is not a valid format or string format"

        if isinstance(end_date, str) and end_date is not None:
            pass
        else: 
            payload["status"] = "error"
            payload["reason"] = "end_date is not a valid format or string format"


        if isinstance(frequency, str) and frequency is not None:
            pass
        else: 
            payload["status"] = "error"
            payload["reason"] = "frequency is not a valid format or string format"

        if isinstance(next_page, str) and next_page is not None:
            pass
        else: 
            payload["status"] = "error"
            payload["reason"] = "next_page is not a valid format or string format"

        rootLogger.info("API is starting 1.0")


        #ENVIRONMENT VARIABLE! After debugging and development remove it.

        intrinio_sdk.ApiClient().configuration.api_key['api_key'] = API_KEY
            
        rootLogger.info("API is starting 2.0")

        security_api = intrinio_sdk.SecurityApi()
        
        rootLogger.info("API is starting 2.0.1")

        try:

            api_response = security_api.get_security_stock_prices(identifier, start_date=start_date, end_date=end_date, frequency=frequency, next_page=next_page)
            
            rootLogger.info("API is starting 2.1")

            #prootLogger.info(api_response)

            rootLogger.info("API is starting 2.2")

            prepared_output = json.dumps(api_response.to_dict(), default = json_datetime_date_convertor)
            
            rootLogger.info("API is starting 2.3")

            output = json.loads(prepared_output)

            rootLogger.info("API is starting 2.4")

            added_counter = 0
            for index, item in enumerate(output["stock_prices"]):

                preview = ApiResponseSecurityStockPrices.query.filter_by(code=output["security"]["code"], composite_ticker = output["security"]["composite_ticker"], frequency = item["frequency"], date=item["date"]).all()
                if preview:
                    rootLogger.info(preview)
                    continue

                rootLogger.info("API is starting 2.4.1")

                api_Response_security_Stock_prices =  ApiResponseSecurityStockPrices(
                    date = item["date"], 
                    intraperiod = item["intraperiod"],
                    frequency = item["frequency"],
                    open = item["open"],
                    high = item["high"],
                    low = item["low"],
                    close = item["close"],
                    volume = item["volume"],
                    adj_open = item["adj_open"],
                    adj_high = item["adj_high"],
                    adj_low = item["adj_low"],
                    adj_close = item["adj_close"],
                    adj_volume = item["adj_volume"],
                    company_id = output["security"]["company_id"],
                    identifier = output["security"]["id"],
                    security_id = output["security"]["id"],
                    name = output["security"]["name"],
                    code = output["security"]["code"],
                    currency =  output["security"]["currency"],
                    ticker = output["security"]["ticker"],
                    composite_ticker = output["security"]["composite_ticker"],
                    figi = output["security"]["figi"],
                    composite_figi = output["security"]["composite_figi"],
                    share_class_figi = output["security"]["share_class_figi"],
                    next_page = output["next_page"]
                )

                db.session.merge(api_Response_security_Stock_prices)
                added_counter = added_counter + 1
            
            rootLogger.info("New elements: {} - Dirty elements: {} - Deleted elements: {}".format(db.session.new,db.session.dirty,db.session.deleted))  # new

            rootLogger.info("API is starting 2.5")

            db.session.commit()

            rootLogger.info("API is starting 4.0")

            payload["data"] = output

            response_time_delta = dat.now() - response_time.start
            payload["stats"] = {"total_elements":added_counter, "response_time":response_time_delta.total_seconds()}

        except Exception as e:

            rootLogger.info("Exception when calling SecurityApi->get_security_stock_prices: %s\n" % e)

            payload = {"result":False}

            payload["reason"] = "API-Exception, please check system logs"
            
            time.sleep(sleep_time)

            return payload
                
        return payload


class intrinioModuleStockPricesBySecurityMassive():
    
    def request(self, identifier, frequency, next_page="", result=[]):

        import time
        response_time = Timer()

        rootLogger.info("API is starting")
        
        # identifier = 'AAPL' # str | A Security identifier (Ticker, FIGI, ISIN, CUSIP, Intrinio ID)
        # start_date = '2018-01-01' # date | Return prices on or after the date (optional)
        # end_date = '2019-01-01' # date | Return prices on or before the date (optional)
        # frequency = 'daily' # str | Return stock prices in the given frequency (optional) (default to daily)
        # next_page = '' # str | Gets the next page of data from a previous API call (optional)

        payload = {"result":True}

        if isinstance(identifier, str) and identifier is not None:
            pass
        else: 
            payload["status"] = "error"
            payload["reason"] = "identifier is not a valid format or string format"

        if isinstance(frequency, str) and frequency is not None:
            pass
        else: 
            payload["status"] = "error"
            payload["reason"] = "frequency is not a valid format or string format"

        intrinio_sdk.ApiClient().configuration.api_key['api_key'] = API_KEY
            
        security_api = intrinio_sdk.SecurityApi()

        try:

            api_response = security_api.get_security_stock_prices(identifier, frequency=frequency, next_page=next_page)
        
            prepared_output = json.dumps(api_response.to_dict(), default = json_datetime_date_convertor)
            
            output = json.loads(prepared_output)

            added_counter = 0
            for index, item in enumerate(output["stock_prices"]):

                preview = ApiResponseSecurityStockPrices.query.filter_by(code=output["security"]["code"], composite_ticker = output["security"]["composite_ticker"], frequency = item["frequency"], date=item["date"]).all()
                if preview:
                    #rootLogger.info(preview)
                    continue

                rootLogger.info("TICKER: {} IN PERIOD: {}".format(output["security"]["composite_ticker"], item["date"]))

                api_Response_security_Stock_prices =  ApiResponseSecurityStockPrices(
                    date = item["date"], 
                    intraperiod = item["intraperiod"],
                    frequency = item["frequency"],
                    open = item["open"],
                    high = item["high"],
                    low = item["low"],
                    close = item["close"],
                    volume = item["volume"],
                    adj_open = item["adj_open"],
                    adj_high = item["adj_high"],
                    adj_low = item["adj_low"],
                    adj_close = item["adj_close"],
                    adj_volume = item["adj_volume"],
                    company_id = output["security"]["company_id"],
                    identifier = output["security"]["id"],
                    security_id = output["security"]["id"],
                    name = output["security"]["name"],
                    code = output["security"]["code"],
                    currency =  output["security"]["currency"],
                    ticker = output["security"]["ticker"],
                    composite_ticker = output["security"]["composite_ticker"],
                    figi = output["security"]["figi"],
                    composite_figi = output["security"]["composite_figi"],
                    share_class_figi = output["security"]["share_class_figi"],
                    next_page = output["next_page"]
                )

                db.session.merge(api_Response_security_Stock_prices)
                added_counter = added_counter + 1
            
            db.session.commit()

            payload["data"] = output

            response_time_delta = dat.now() - response_time.start

            payload["stats"] = {"total_elements":added_counter, "response_time":response_time_delta.total_seconds()}

            if output["next_page"] is None:
                
                rootLogger.info("ENDING T: {}".format(output["security"]["composite_ticker"]))
                return payload

            else:

                result.append(payload["data"])

                rootLogger.info("APPEND T: {}".format(output["security"]["composite_ticker"]))
                self.request(identifier=identifier, frequency=frequency, next_page=output["next_page"], result=result)
                
        except Exception as e:

            rootLogger.info("Exception when calling SecurityApi->get_security_stock_prices: %s\n" % e)

            payload = {"result":False}

            payload["reason"] = "API-Exception, please check system logs"
            
            time.sleep(sleep_time)

            return payload
                






class intrinioModuleRequesterSystem():

    def get_all_companies(self):
        
        list_companies = ApiResponseCompaniesList.query.all()

        if not list_companies:

            rootLogger.info("No companies found")
            
            return []

        return list_companies


    def request(self):

        response_time = Timer()

        payload = {"result":True}

        try:

            stocks_prices_by_security = intrinioModuleStockPricesBySecurityMassive()

            payload["companies_tickers"] =  self.get_all_companies()

            response = []

            for company in payload["companies_tickers"]:

                selected = random.choice(payload["companies_tickers"])

                for period in ["daily", "weekly", "monthly"]:

                    print("COMPANY: {} IN MODE: {}".format(selected.ticker, period))

                    response.append(stocks_prices_by_security.request(identifier=selected.ticker, frequency=period))
                    

        except Exception as e:

            rootLogger.info("Exception when calling CompanyApi->get_all_companies: %s\n" % e)

            payload = {"result":False}

            payload["reason"] = "API-Exception, please check system logs"

            time.sleep(sleep_time)

            return payload
                
        return payload


############################################ SECOND API ###############################################


class intrinioModuleStockPricesBySecurityAdjustments():
    
    def request(self, identifier, start_date, end_date, next_page):

        response_time = Timer()

        # identifier = 'AAPL' # str | A Security identifier (Ticker, FIGI, ISIN, CUSIP, Intrinio ID)
        # start_date = '2018-01-01' # date | Return prices on or after the date (optional)
        # end_date = '2019-01-01' # date | Return prices on or before the date (optional)
        # next_page = '' # str | Gets the next page of data from a previous API call (optional)

        payload = {"result":True}

        if isinstance(identifier, str) and identifier is not None:
            pass
        else: 
            payload["status"] = "error"
            payload["reason"] = "identifier is not a valid format or string format"

        if isinstance(start_date, str) and start_date is not None:
            pass
        else: 
            payload["status"] = "error"
            payload["reason"] = "start_date is not a valid format or string format"

        if isinstance(end_date, str) and end_date is not None:
            pass
        else: 
            payload["status"] = "error"
            payload["reason"] = "end_date is not a valid format or string format"

        if isinstance(next_page, str) and next_page is not None:
            pass
        else: 
            payload["status"] = "error"
            payload["reason"] = "next_page is not a valid format or string format"

        intrinio_sdk.ApiClient().configuration.api_key['api_key'] = API_KEY
        
        security_api = intrinio_sdk.SecurityApi()
    
        try:

            api_response = security_api.get_security_stock_price_adjustments(identifier, start_date=start_date, end_date=end_date,  next_page=next_page)
            
            prepared_output = json.dumps(api_response.to_dict(), default = json_datetime_date_convertor)
            
            output = json.loads(prepared_output)
            
            added_counter = 0
            for index, item in enumerate(output["stock_price_adjustments"]):

                preview = ApiResponseSecurityStockPricesAdjustments.query.filter_by(code=output["security"]["code"], composite_ticker = output["security"]["composite_ticker"], date=item["date"]).all()
                if preview:
                    rootLogger.info(preview)
                    continue

                api_Response_security_Stock_prices =  ApiResponseSecurityStockPricesAdjustments(

                    date = item["date"], 
                    dividend_currency = item["dividend_currency"],
                    split_ratio = item["split_ratio"],
                    factor = item["factor"],
                    dividend = item["dividend"],    

                    company_id = output["security"]["company_id"],
                    identifier = output["security"]["id"],
                    security_id = output["security"]["id"],
                    name = output["security"]["name"],
                    code = output["security"]["code"],
                    currency =  output["security"]["currency"],
                    ticker = output["security"]["ticker"],
                    composite_ticker = output["security"]["composite_ticker"],
                    figi = output["security"]["figi"],
                    composite_figi = output["security"]["composite_figi"],
                    share_class_figi = output["security"]["share_class_figi"],
                    next_page = output["next_page"]
                )

                db.session.merge(api_Response_security_Stock_prices)

                added_counter = added_counter + 1
            
            db.session.commit()

            payload["data"] = output

            response_time_delta = dat.now() - response_time.start

            payload["stats"] = {"total_elements":added_counter, "response_time":response_time_delta.total_seconds()}

        except Exception as e:

            rootLogger.info("Exception when calling SecurityApi->get_security_stock_prices: %s\n" % e)

            payload = {"result":False}

            payload["reason"] = "API-Exception, please check system logs"

            time.sleep(sleep_time)

            return payload
                
        return payload



class intrinioModuleStockPricesBySecurityAdjustmentsMassive():
    
    def request(self, identifier, next_page="", result=[]):

        response_time = Timer()

        # identifier = 'AAPL' # str | A Security identifier (Ticker, FIGI, ISIN, CUSIP, Intrinio ID)
        # next_page = '' # str | Gets the next page of data from a previous API call (optional)

        payload = {"result":True}

        if isinstance(identifier, str) and identifier is not None:
            pass
        else: 
            payload["status"] = "error"
            payload["reason"] = "identifier is not a valid format or string format"

        if isinstance(next_page, str) and next_page is not None:
            pass
        else: 
            payload["status"] = "error"
            payload["reason"] = "next_page is not a valid format or string format"

        intrinio_sdk.ApiClient().configuration.api_key['api_key'] = API_KEY
        
        security_api = intrinio_sdk.SecurityApi()
    
        try:

            api_response = security_api.get_security_stock_price_adjustments(identifier,  next_page=next_page)
            
            prepared_output = json.dumps(api_response.to_dict(), default = json_datetime_date_convertor)
            
            output = json.loads(prepared_output)
            
            added_counter = 0

            for index, item in enumerate(output["stock_price_adjustments"]):

                preview = ApiResponseSecurityStockPricesAdjustments.query.filter_by(code=output["security"]["code"], composite_ticker = output["security"]["composite_ticker"], date=item["date"]).all()
                if preview:
                    rootLogger.info(preview)
                    continue

                rootLogger.info("TICKER: {} IN DATE: {}".format(output["security"]["composite_ticker"], item["date"]))

                api_Response_security_Stock_prices =  ApiResponseSecurityStockPricesAdjustments(

                    date = item["date"], 
                    dividend_currency = item["dividend_currency"],
                    split_ratio = item["split_ratio"],
                    factor = item["factor"],
                    dividend = item["dividend"],    

                    company_id = output["security"]["company_id"],
                    identifier = output["security"]["id"],
                    security_id = output["security"]["id"],
                    name = output["security"]["name"],
                    code = output["security"]["code"],
                    currency =  output["security"]["currency"],
                    ticker = output["security"]["ticker"],
                    composite_ticker = output["security"]["composite_ticker"],
                    figi = output["security"]["figi"],
                    composite_figi = output["security"]["composite_figi"],
                    share_class_figi = output["security"]["share_class_figi"],
                    next_page = output["next_page"]
                )

                db.session.merge(api_Response_security_Stock_prices)

                added_counter = added_counter + 1
            
            db.session.commit()

            payload["data"] = output

            response_time_delta = dat.now() - response_time.start

            payload["stats"] = {"total_elements":added_counter, "response_time":response_time_delta.total_seconds()}

            if output["next_page"] is None:
                
                rootLogger.info("ENDING T: {}".format(output["security"]["composite_ticker"]))
                return payload

            else:

                result.append(payload["data"])

                rootLogger.info("APPEND T: {}".format(output["security"]["composite_ticker"]))
                self.request(identifier=identifier, next_page=output["next_page"], result=result)
                
        except Exception as e:

            rootLogger.info("Exception when calling SecurityApi->get_security_stock_price_adjustments: %s\n" % e)

            payload = {"result":False}

            payload["reason"] = "API-Exception, please check system logs"

            time.sleep(sleep_time)

            return payload



class StockPricesBySecurityAdjustmentsRS():

    def get_all_companies(self):
        
        list_companies = ApiResponseCompaniesList.query.all()

        if not list_companies:

            rootLogger.info("No companies found")
            
            return []

        return list_companies


    def request(self):

        response_time = Timer()

        payload = {"result":True}

        try:

            stocks_prices_by_security_adjustments = intrinioModuleStockPricesBySecurityAdjustmentsMassive()

            payload["companies_tickers"] =  self.get_all_companies()

            response = []

            for company in payload["companies_tickers"]:

                selected = random.choice(payload["companies_tickers"])

                print("COMPANY: {}".format(selected.ticker))

                response.append(stocks_prices_by_security_adjustments.request(identifier=selected.ticker))

                print("COMPANY: {} TOTAL AGREGATE ROWS: {}".format(selected.ticker,len(response)))

                    

        except Exception as e:

            rootLogger.info("Exception when calling CompanyApi->get_all_companies: %s\n" % e)

            payload = {"result":False}

            payload["reason"] = "API-Exception, please check system logs"

            time.sleep(sleep_time)

            return payload
                
        return payload





############################################ THIRD API ###############################################



class intrinioModuleFundamentalStandarizedFinancialsMassive():
    
    def request(self, identifier, next_page="", result=[]):

        response_time = Timer()

        # identifier = 'AAPL' # str | A Security identifier (Ticker, FIGI, ISIN, CUSIP, Intrinio ID)
        # next_page = '' # str | Gets the next page of data from a previous API call (optional)

        payload = {"result":True}

        if isinstance(identifier, str) and identifier is not None:
            pass
        else: 
            payload["status"] = "error"
            payload["reason"] = "identifier is not a valid format or string format"

        if isinstance(next_page, str) and next_page is not None:
            pass
        else: 
            payload["status"] = "error"
            payload["reason"] = "next_page is not a valid format or string format"

        intrinio_sdk.ApiClient().configuration.api_key['api_key'] = API_KEY
        
        fundamentals_api = intrinio_sdk.FundamentalsApi()    

        try:

            api_response = fundamentals_api.get_fundamental_standardized_financials(identifier) # next_page is not used, please validate.
            
            prepared_output = json.dumps(api_response.to_dict(), default = json_datetime_date_convertor)
            
            output = json.loads(prepared_output)
            
            added_counter = 0

            for index, item in enumerate(output["standardized_financials"]):

                preview = ApiResponseStandardizedFinancials.query.filter_by(tag_id=item["data_tag"]["id"], tag_name=item["data_tag"]["name"], fundamental_id=output["fundamental"]["id"], start_date=output["fundamental"]["start_date"], end_date=output["fundamental"]["end_date"]).all()
                if preview:
                    rootLogger.info(preview)
                    continue

                rootLogger.info("FUNDAMENTAL ID: {} IN TAG ID: {}".format(output["fundamental"]["id"], item["data_tag"]["id"]))

                api_response_fundamentals_standarized =  ApiResponseStandardizedFinancials(


                    data_tag = True if item else False,

                    tag_id = item["data_tag"]["id"],
                    tag_name = item["data_tag"]["name"],
                    tag = item["data_tag"]["tag"],
                    unit = item["data_tag"]["unit"],
                    value = item["value"],

                    fundamental_id = output["fundamental"]["id"], 
                    statement_code = output["fundamental"]["statement_code"],
                    fiscal_year = output["fundamental"]["fiscal_year"],
                    fiscal_period = output["fundamental"]["fiscal_period"],
                    type = output["fundamental"]["type"],    
                    start_date = output["fundamental"]["start_date"],
                    end_date = output["fundamental"]["end_date"],
                    filing_date = output["fundamental"]["filing_date"], 

                    company = True if output.get("company", False) else False,
                    intrinio_id = output.get("company", {}).get("id", None),
                    name = output.get("company", {}).get("name", None),
                    ticker = output.get("company", {}).get("ticker", None),
                    lei = output.get("company", {}).get("lei", None),
                    cik = output.get("company", {}).get("cik", None),

                    next_page = output["next_page"]

                    )


                db.session.merge(api_response_fundamentals_standarized)

                added_counter = added_counter + 1
            
            db.session.commit()

            payload["data"] = output

            response_time_delta = dat.now() - response_time.start

            payload["stats"] = {"total_elements":added_counter, "response_time":response_time_delta.total_seconds()}

            if output["next_page"] is None:
                
                rootLogger.info("ENDING T: {}".format(output["fundamental"]["id"]))
                return payload

            else:

                result.append(payload["data"])

                rootLogger.info("APPEND T: {}".format(output["fundamental"]["id"]))
                self.request(identifier=identifier, next_page=output["next_page"], result=result)
                
        except Exception as e:

            rootLogger.info("Exception when calling SecurityApi->api_response_fundamentals_standarized: %s\n" % e)

            payload = {"result":False}

            payload["reason"] = "API-Exception, please check system logs"
            
            time.sleep(sleep_time)

            return payload


class FundamentalStandarizedFinancialsRS():

    def get_all_fundamentals(self):
        
        list_fundamentals_ids= ApiResponseCompanyFundamentals.query.all()

        if not list_fundamentals_ids:

            rootLogger.info("No fundamentals found")
            
            return []

        return list_fundamentals_ids


    def request(self):

        response_time = Timer()

        payload = {"result":True}

        try:

            fundamentals_standarized_financials = intrinioModuleFundamentalStandarizedFinancialsMassive()

            payload["fundamentals"] =  self.get_all_fundamentals()

            response = []

            for fundamental_id in payload["fundamentals"]:

                fundamental_selected = random.choice(payload["fundamentals"])

                print("FUNDAMENTAL ID: {} TOTAL OF FUND: {}".format(fundamental_selected.fundamental_id, len(payload["fundamentals"])))

                response.append(fundamentals_standarized_financials.request(identifier=fundamental_selected.fundamental_id))

                print("FUNDAMENTAL ID: {} TOTAL AGREGATE ROWS: {}".format(fundamental_selected.fundamental_id,len(response)))

                    

        except Exception as e:

            rootLogger.info("Exception when calling CompanyApi->get_all_companies: %s\n" % e)

            payload = {"result":False}

            payload["reason"] = "API-Exception, please check system logs"
            
            time.sleep(sleep_time)

            return payload
                
        return payload


############################################ Lookup Fundamental  API ###############################################



class intrinioModuleCompaniesFundamentals():

    def get_fundamentals_information(self, identifier, next_page="", result=[]):

        response_time = Timer()

        #identifier = 'AAPL' # str | A Company identifier (Ticker, CIK, LEI, Intrinio ID)
        #filed_after = '' # date | Filed on or after this date (optional)
        #filed_before = '' # date | Filed on or before this date (optional)
        #reported_only = False # bool | Only as-reported fundamentals (optional)
        #fiscal_year = 2017 # int | Only for the given fiscal year (optional)
        #statement_code = '' # str | Only of the given statement code (optional)
        #type = '' # str | Only of the given type (optional)
        #start_date = '' # date | Only on or after the given date (optional)
        #end_date = '' # date | Only on or before the given date (optional)
        #page_size = 1000 # float | The number of results to return (optional) (default to 100)
        #next_page = '' # str | Gets the next page of data from a previous API call (optional)

        payload = {"result":True}

        if isinstance(identifier, str) and identifier is not None:
            pass
        else: 
            payload["status"] = "error"
            payload["reason"] = "identifier is not a valid format or string format"

        if isinstance(next_page, str) and next_page is not None:
            pass
        else: 
            payload["status"] = "error"
            payload["reason"] = "next_page is not a valid format or string format"

        
        intrinio_sdk.ApiClient().configuration.api_key['api_key'] = API_KEY

        company_api = intrinio_sdk.CompanyApi()

        try:

            api_response = company_api.get_company_fundamentals(identifier, next_page=next_page)
            
            prepared_output = json.dumps(api_response.to_dict(), default = json_datetime_date_convertor)
            
            output = json.loads(prepared_output)

            added_counter = 0

            for index, item in enumerate(output["fundamentals"]):

                preview = ApiResponseCompanyFundamentals.query.filter_by(lei=output["company"]["lei"], ticker = output["company"]["ticker"], fundamental_id=item["id"], start_date=item["start_date"], end_date=item["end_date"]).all()
                if preview:
                    rootLogger.info(preview)
                    continue

                rootLogger.info("TICKER: {} FUNDAMENTAL ID: {}".format(output["company"]["ticker"], item["id"]))

                api_response_company_fundamentals =  ApiResponseCompanyFundamentals(

                    fundamental_id = item["id"], 
                    statement_code = item["statement_code"],
                    fiscal_year = item["fiscal_year"],
                    fiscal_period = item["fiscal_period"],
                    type = item["type"],    
                    start_date = item["start_date"],
                    end_date = item["end_date"],
                    filing_date = item["filing_date"], 

                    intrinio_id = output["company"]["id"],
                    name = output["company"]["name"],
                    ticker = output["company"]["ticker"],
                    lei = output["company"]["lei"],
                    cik = output["company"]["cik"],

                    next_page = output["next_page"]
                    )

                db.session.merge(api_response_company_fundamentals)

                added_counter = added_counter + 1
            
            db.session.commit()

            payload["data"] = output

            response_time_delta = dat.now() - response_time.start

            payload["stats"] = {"total_elements":added_counter, "response_time":response_time_delta.total_seconds()}

            if output["next_page"] is None:
                
                rootLogger.info("ENDING T: {}".format(output["company"]["ticker"]))

                return payload

            else:

                result.append(payload["data"])

                rootLogger.info("APPEND T: {}".format(output["company"]["ticker"]))

                self.get_fundamentals_information(identifier=identifier, next_page=output["next_page"], result=result)
                
        except Exception as e:

            rootLogger.info("Exception when calling SecurityApi->api_response_company_fundamentals: %s\n" % e)

            payload = {"result":False}

            payload["reason"] = "API-Exception, please check system logs"

            time.sleep(sleep_time)

            return payload

    
    def get_all_companies(self):
        
        list_companies = ApiResponseCompaniesList.query.all()

        if not list_companies:

            rootLogger.info("No companies found")
            
            return []

        return list_companies


    def request(self):

        response_time = Timer()

        payload = {"result":True}

        try:

            payload["companies_tickers"] =  self.get_all_companies()

            response = []

            for company in payload["companies_tickers"]:

                selected = random.choice(payload["companies_tickers"])

                print("COMPANY: {}".format(selected.ticker))

                response.append(self.get_fundamentals_information(identifier=selected.ticker))

                print("COMPANY: {} TOTAL AGREGATE ROWS: {}".format(selected.ticker,len(response)))

                    

        except Exception as e:

            rootLogger.info("Exception when calling CompanyApi->get_all_companies: %s\n" % e)

            payload = {"result":False}

            payload["reason"] = "API-Exception, please check system logs"
            
            time.sleep(sleep_time)

            return payload
                
        return payload


############################################ 7TH API COMPANY NEWS ###############################################



class intrinioModuleCompanyNewsMassive():
    
    def request(self, identifier, next_page="", result=[]):

        response_time = Timer()

        #identifier = 'AAPL' # str | A Company identifier (Ticker, CIK, LEI, Intrinio ID)
        #page_size = 100 # float | The number of results to return (optional) (default to 100)
        #next_page = '' # str | Gets the next page of data from a previous API call (optional)

        payload = {"result":True}

        if isinstance(identifier, str) and identifier is not None:
            pass
        else: 
            payload["status"] = "error"
            payload["reason"] = "identifier is not a valid format or string format"

        if isinstance(next_page, str) and next_page is not None:
            pass
        else: 
            payload["status"] = "error"
            payload["reason"] = "next_page is not a valid format or string format"


        intrinio_sdk.ApiClient().configuration.api_key['api_key'] = API_KEY
        
        company_api = intrinio_sdk.CompanyApi()

        try:

            api_response = company_api.get_company_news(identifier, next_page=next_page)
            
            prepared_output = json.dumps(api_response.to_dict(), default = json_datetime_date_convertor)
            
            output = json.loads(prepared_output)
            
            added_counter = 0

            for index, item in enumerate(output["news"]):

                preview = ApiResponseCompanyNews.query.filter_by(news_id=item["id"], lei=output["company"]["lei"], name=output["company"]["name"]).all()
                if preview:
                    rootLogger.info(preview)
                    continue

                rootLogger.info("NEWS ID: {} IN DATE ID: {}".format(item["id"], item["publication_date"]))

                api_response_company_news =  ApiResponseCompanyNews(

                    news_id = item["id"],
                    publication_date = item["publication_date"],
                    summary = item["summary"],
                    title = item["title"],
                    url = item["url"],

                    intrinio_id = output["company"]["id"],
                    name = output["company"]["name"],
                    ticker = output["company"]["ticker"],
                    lei = output["company"]["lei"],
                    cik = output["company"]["cik"],

                    next_page = output["next_page"])


                db.session.merge(api_response_company_news)

                added_counter = added_counter + 1
            
            db.session.commit()

            payload["data"] = output

            response_time_delta = dat.now() - response_time.start

            payload["stats"] = {"total_elements":added_counter, "response_time":response_time_delta.total_seconds()}

            if output["next_page"] is None:
                
                rootLogger.info("ENDING T: {}".format(output["company"]["ticker"]))
                return payload

            else:

                result.append(payload["data"])

                rootLogger.info("APPEND T: {}".format(output["company"]["ticker"]))
                self.request(identifier=identifier, next_page=output["next_page"], result=result)
                
        except Exception as e:

            rootLogger.info("Exception when calling SecurityApi->get_company_news: %s\n" % e)

            payload = {"result":False}

            payload["reason"] = "API-Exception, please check system logs"
            
            time.sleep(sleep_time)

            return payload


class CompanyNewsRS():

    def get_all_companies(self):
        
        list_companies = ApiResponseCompaniesList.query.all()

        if not list_companies:

            rootLogger.info("No companies found")
            
            return []

        return list_companies


    def request(self):

        response_time = Timer()

        payload = {"result":True}

        try:

            company_news = intrinioModuleCompanyNewsMassive()

            payload["companies"] =  self.get_all_companies()

            response = []

            for company in payload["companies"]:

                company_selected = random.choice(payload["companies"])

                print("COMPANY TICKER: {} TOTAL OF COMPANIES: {}".format(company_selected.ticker, len(payload["companies"])))

                response.append(company_news.request(identifier=company_selected.ticker))

                print("COMPANY TICKER: {} TOTAL AGREGATE ROWS: {}".format(company_selected.ticker,len(response)))

                    

        except Exception as e:

            rootLogger.info("Exception when calling CompanyApi->CompanyNewsRS: %s\n" % e)

            payload = {"result":False}

            payload["reason"] = "API-Exception, please check system logs"
            
            time.sleep(sleep_time)

            return payload
                
        return payload


############################################ 5TH API get_security_latest_dividend_record ###############################################


class SecurityLatestDividendRecordRS():

    def get_all_companies(self):
        
        list_companies = ApiResponseCompaniesList.query.all()

        if not list_companies:

            rootLogger.info("No companies found")
            
            return []

        return list_companies


    def request(self):

        response_time = Timer()

        payload = {"result":True}

        try:

            #company_news = intrinioModuleCompanyNewsMassive()

            payload["companies"] =  self.get_all_companies()

            response = []

            for company in payload["companies"]:

                company_selected = random.choice(payload["companies"])

                print("TICKER: {} TOTAL OF COMPANIES: {}".format(company_selected.ticker, len(payload["companies"])))

                #response.append(company_news.request(identifier=company_selected.ticker))

                print("TICKER: {} TOTAL AGREGATE ROWS: {}".format(company_selected.ticker,len(response)))

                    

        except Exception as e:

            rootLogger.info("Exception when calling CompanyApi->SecurityLatestDividendRecordRS: %s\n" % e)

            payload = {"result":False}

            payload["reason"] = "API-Exception, please check system logs"

            time.sleep(sleep_time)

            return payload
                
        return payload


############################################ 6TH API get_security_latest_earnings_record ###############################################

class SecurityLatestDividendEarningsRS():

    def get_all_companies(self):
        
        list_companies = ApiResponseCompaniesList.query.all()

        if not list_companies:

            rootLogger.info("No companies found")
            
            return []

        return list_companies


    def request(self):

        response_time = Timer()

        payload = {"result":True}

        try:

            #company_news = intrinioModuleCompanyNewsMassive()

            payload["companies"] =  self.get_all_companies()

            response = []

            for company in payload["companies"]:

                company_selected = random.choice(payload["companies"])

                print("TICKER: {} TOTAL OF COMPANIES: {}".format(company_selected.ticker, len(payload["companies"])))

                #response.append(company_news.request(identifier=company_selected.ticker))

                print("TICKER: {} TOTAL AGREGATE ROWS: {}".format(company_selected.ticker,len(response)))

                    

        except Exception as e:


            rootLogger.info("Exception when calling CompanyApi->SecurityLatestDividendEarningsRS: %s\n" % e)

            payload = {"result":False}

            payload["reason"] = "API-Exception, please check system logs"

            time.sleep(sleep_time)

            return payload
                
        return payload


############################################ 8TH API CompanyInformation  ###############################################




class CompanyMassive():
    
    def request(self, identifier, next_page="", result=[]):

        response_time = Timer()

        #identifier = 'AAPL' # str | A Company identifier (Ticker, CIK, LEI, Intrinio ID)
        #page_size = 100 # float | The number of results to return (optional) (default to 100)
        #next_page = '' # str | Gets the next page of data from a previous API call (optional)

        payload = {"result":True}

        if isinstance(identifier, str) and identifier is not None:
            pass
        else: 
            payload["status"] = "error"
            payload["reason"] = "identifier is not a valid format or string format"

        if isinstance(next_page, str) and next_page is not None:
            pass
        else: 
            payload["status"] = "error"
            payload["reason"] = "next_page is not a valid format or string format"


        url = "https://api-v2.intrinio.com/companies/{}"

        querystring = {"api_key":API_KEY}

        payload = ""
        headers = {
            'cache-control': "no-cache",
            'Postman-Token': "f2738443-9718-45d6-bd4b-b9e37c8e7d90"
            }


        try:


            response = requests.request("GET", url.format(identifier), data=payload, headers=headers, params=querystring)

            json_response = json.loads(response.text)
        
            prepared_output = json.dumps(json_response, default = json_datetime_date_convertor)
            
            output = json.loads(prepared_output)
            
            added_counter = 0

            preview = ApiResponseCompany.query.filter_by(latest_filing_date=output["latest_filing_date"], ticker=output["ticker"], intrinio_id=output["id"], lei=output["lei"], name=output["name"]).all()
            
            if preview:
                rootLogger.info(preview)
                return False

            rootLogger.info("NEWS ID: {} IN DATE ID: {}".format(output["id"], output["latest_filing_date"]))

            api_response_us_insider =  ApiResponseCompany(

                intrinio_id = output["id"],
                ticker = output["ticker"],
                name = output["name"],
                lei = output["lei"],
                cik = output["cik"],

                legal_name = output["legal_name"],
                stock_exchange = output["stock_exchange"],
                sic  = output["sic"],
                short_description = output["short_description"],
                ceo = output["ceo"],
                company_url = output["company_url"],
                business_address = output["business_address"],
                mailing_address = output["mailing_address"],
                business_phone_no = output["business_phone_no"],
                hq_address1 = output["hq_address1"],
                hq_address2 = output["hq_address2"],
                hq_address_city = output["hq_address_city"],
                hq_address_postal_code = output["hq_address_postal_code"],
                entity_legal_form = output["entity_legal_form"],

                latest_filing_date = output["latest_filing_date"],
                hq_state = output["hq_state"],
                hq_country = output["hq_country"],
                inc_state = output["inc_state"],
                inc_country = output["inc_country"],
                employees  = output["employees"],
                entity_status = output["entity_status"],
                sector = output["sector"],
                industry_category = output["industry_category"],
                industry_group = output["industry_group"],
                template = output["template"],
                standardized_active = output["standardized_active"],

            )

            db.session.merge(api_response_us_insider)

            added_counter = added_counter + 1
            
            db.session.commit()

            payload["data"] = output

            response_time_delta = dat.now() - response_time.start

            payload["stats"] = {"total_elements":added_counter, "response_time":response_time_delta.total_seconds()}

            if output["next_page"] is None:
                
                rootLogger.info("ENDING T: {}".format(output["ticker"]))
                return payload

            else:

                result.append(payload["data"])

                rootLogger.info("APPEND T: {}".format(output["ticker"]))
                self.request(identifier=identifier, next_page=output["next_page"], result=result)
                
        except Exception as e:

            rootLogger.info("Exception when calling ApiResponseUSInsiderTransactionsOwnership->api_response_us_insider: %s\n" % e)

            payload = {"result":False}

            payload["reason"] = "API-Exception, please check system logs"
            
            time.sleep(sleep_time)

            return payload


class CompanyRS():


    def get_all_companies(self):
        
        list_companies = ApiResponseCompaniesList.query.all()

        if not list_companies:

            rootLogger.info("No companies found")
            
            return []

        return list_companies


    def request(self):

        response_time = Timer()

        payload = {"result":True}

        try:

            us_insider_company = CompanyMassive()

            payload["companies"] =  self.get_all_companies()

            response = []

            for company in payload["companies"]:

                company_selected = random.choice(payload["companies"])

                print("8THAPI: TICKER: {} TOTAL OF COMPANIES: {}".format(company_selected.ticker, len(payload["companies"])))

                result = us_insider_company.request(identifier=company_selected.ticker) if us_insider_company.request(identifier=company_selected.ticker) else False
                
                if result:

                    response.append()
                
                else:

                    print("8THAPI: DUPLICATE: {} RESULT: {}".format(company_selected.ticker,result))


                print("8THAPI: TICKER: {} TOTAL AGREGATE ROWS: {}".format(company_selected.ticker,len(response)))

                    

        except Exception as e:


            rootLogger.info("Exception when calling CompanyApi->USInsiderTransactionsOwnershipMassive: %s\n" % e)

            payload = {"result":False}

            payload["reason"] = "API-Exception, please check system logs"

            time.sleep(sleep_time)

            return payload
                
        return payload

############################################ 9TH API US insider Transactions  ###############################################





class USInsiderTransactionsRS():


    def get_all_companies(self):
        
        list_companies = ApiResponseCompaniesList.query.all()

        if not list_companies:

            rootLogger.info("No companies found")
            
            return []

        return list_companies


    def request(self):

        response_time = Timer()

        payload = {"result":True}

        try:

            #us_insider_company = CompanyMassive()

            payload["companies"] =  self.get_all_companies()

            response = []

            for company in payload["companies"]:

                company_selected = random.choice(payload["companies"])

                print("9THAPI: TICKER: {} TOTAL OF COMPANIES: {}".format(company_selected.ticker, len(payload["companies"])))


                print("9THAPI: TICKER: {} TOTAL AGREGATE ROWS: {}".format(company_selected.ticker,len(response)))

                    

        except Exception as e:


            rootLogger.info("Exception when calling CompanyApi->USInsiderTransactionsOwnershipMassive: %s\n" % e)

            payload = {"result":False}

            payload["reason"] = "API-Exception, please check system logs"

            time.sleep(sleep_time)

            return payload
                
        return payload


############################################ 10TH API US insider Ownership  ###############################################


class USInsiderOwnershipMassive():
    
    def request(self, identifier, next_page="", result=[]):

        response_time = Timer()

        #identifier = 'AAPL' # str | A Company identifier (Ticker, CIK, LEI, Intrinio ID)
        #page_size = 100 # float | The number of results to return (optional) (default to 100)
        #next_page = '' # str | Gets the next page of data from a previous API call (optional)

        payload = {"result":True}

        if isinstance(identifier, str) and identifier is not None:
            pass
        else: 
            payload["status"] = "error"
            payload["reason"] = "identifier is not a valid format or string format"

        if isinstance(next_page, str) and next_page is not None:
            pass
        else: 
            payload["status"] = "error"
            payload["reason"] = "next_page is not a valid format or string format"


        url = "https://api-v2.intrinio.com/companies/{}"

        querystring = {"api_key":API_KEY}

        payload = ""
        headers = {
            'cache-control': "no-cache",
            'Postman-Token': "f2738443-9718-45d6-bd4b-b9e37c8e7d90"
            }


        try:


            response = requests.request("GET", url.format(identifier), data=payload, headers=headers, params=querystring)

            json_response = json.loads(response.text)
        
            prepared_output = json.dumps(json_response, default = json_datetime_date_convertor)
            
            output = json.loads(prepared_output)
            
            added_counter = 0

            preview = ApiResponseCompany.query.filter_by(latest_filing_date=output["latest_filing_date"], ticker=output["ticker"], intrinio_id=output["id"], lei=output["lei"], name=output["name"]).all()
            
            if preview:
                rootLogger.info(preview)
                return False

            rootLogger.info("NEWS ID: {} IN DATE ID: {}".format(output["id"], output["latest_filing_date"]))

            api_response_us_insider =  ApiResponseCompany(

                intrinio_id = output["id"],
                ticker = output["ticker"],
                name = output["name"],
                lei = output["lei"],
                cik = output["cik"],

                legal_name = output["legal_name"],
                stock_exchange = output["stock_exchange"],
                sic  = output["sic"],
                short_description = output["short_description"],
                ceo = output["ceo"],
                company_url = output["company_url"],
                business_address = output["business_address"],
                mailing_address = output["mailing_address"],
                business_phone_no = output["business_phone_no"],
                hq_address1 = output["hq_address1"],
                hq_address2 = output["hq_address2"],
                hq_address_city = output["hq_address_city"],
                hq_address_postal_code = output["hq_address_postal_code"],
                entity_legal_form = output["entity_legal_form"],

                latest_filing_date = output["latest_filing_date"],
                hq_state = output["hq_state"],
                hq_country = output["hq_country"],
                inc_state = output["inc_state"],
                inc_country = output["inc_country"],
                employees  = output["employees"],
                entity_status = output["entity_status"],
                sector = output["sector"],
                industry_category = output["industry_category"],
                industry_group = output["industry_group"],
                template = output["template"],
                standardized_active = output["standardized_active"],

            )

            db.session.merge(api_response_us_insider)

            added_counter = added_counter + 1
            
            db.session.commit()

            payload["data"] = output

            response_time_delta = dat.now() - response_time.start

            payload["stats"] = {"total_elements":added_counter, "response_time":response_time_delta.total_seconds()}

            if output["next_page"] is None:
                
                rootLogger.info("ENDING T: {}".format(output["ticker"]))
                return payload

            else:

                result.append(payload["data"])

                rootLogger.info("APPEND T: {}".format(output["ticker"]))
                self.request(identifier=identifier, next_page=output["next_page"], result=result)
                
        except Exception as e:

            rootLogger.info("Exception when calling ApiResponseUSInsiderTransactionsOwnership->api_response_us_insider: %s\n" % e)

            payload = {"result":False}

            payload["reason"] = "API-Exception, please check system logs"
            
            time.sleep(sleep_time)

            return payload


class USInsiderOwnershipRS():


    def get_all_companies(self):
        
        list_companies = ApiResponseCompaniesList.query.all()

        if not list_companies:

            rootLogger.info("No companies found")
            
            return []

        return list_companies


    def request(self):

        response_time = Timer()

        payload = {"result":True}

        try:

            #us_insider_company = CompanyMassive()

            payload["companies"] =  self.get_all_companies()

            response = []

            for company in payload["companies"]:


                print("10THAPI: TICKER: {} TOTAL OF COMPANIES: {}".format(company.ticker, len(payload["companies"])))


                    

        except Exception as e:


            rootLogger.info("Exception when calling CompanyApi->USInsiderTransactionsOwnershipMassive: %s\n" % e)

            payload = {"result":False}

            payload["reason"] = "API-Exception, please check system logs"

            time.sleep(sleep_time)

            return payload
                
        return payload

############################################ 11TH API Security Bulk Version  ###############################################


class SecurityStocksPricesBulkRS():


    def get_next_company(self):
        
        list_companies = ApiResponseCompaniesList.query.filter_by(complete_1=None, error_1=None, progress_1=None).all()

        if not list_companies:

            rootLogger.info("No companies found")
            
            return []

        return list_companies[0]

    def bulk_save(self, ticker):

        try: 
            
            url = "https://api-v2.intrinio.com/securities/{}/prices"

            querystring = {"api_key":API_KEY, "identifier":ticker, "bulk":True}

            payload_request = ""
            headers = {
                'cache-control': "no-cache",
                'Postman-Token': "f2738443-9718-45d6-bd4b-b9e37c8e7d90"
            }

            response = requests.request("GET", url.format(ticker), data=payload_request, headers=headers, params=querystring)

            payload = {"result":True}

            string_data = "["+response.text.split("[")[1].split("]")[0]+"]"

            bulk_data = json.loads(literal_eval("'%s'" % string_data))

            intrinio_sdk.ApiClient().configuration.api_key['api_key'] = API_KEY
            
            rootLogger.info("API SDK intrinio!")

            security_api = intrinio_sdk.SecurityApi()

            api_response = security_api.get_security_stock_prices(identifier=ticker)

            prepared_output = json.dumps(api_response.to_dict(), default = json_datetime_date_convertor)
            
            output = json.loads(prepared_output)

            added_counter = 0
            for index, item in enumerate(bulk_data):

                preview = ApiResponseSecurityStockPrices.query.filter_by(code=output["security"]["code"], composite_ticker = output["security"]["composite_ticker"], frequency = item["frequency"], date=item["date"]).all()
                if preview:
                    rootLogger.info("{}/{} DUPLICATED: {}".format(index, len(bulk_data),preview))
                    continue

                rootLogger.info("{}/{} TICKER: {} IN PERIOD: {}".format(index, len(bulk_data), output["security"]["composite_ticker"], item["date"]))

                api_Response_security_Stock_prices =  ApiResponseSecurityStockPrices(
                    date = item["date"], 
                    intraperiod = item["intraperiod"],
                    frequency = item["frequency"],
                    open = item["open"],
                    high = item["high"],
                    low = item["low"],
                    close = item["close"],
                    volume = item["volume"],
                    adj_open = item["adj_open"],
                    adj_high = item["adj_high"],
                    adj_low = item["adj_low"],
                    adj_close = item["adj_close"],
                    adj_volume = item["adj_volume"],
                    company_id = output["security"]["company_id"],
                    identifier = output["security"]["id"],
                    security_id = output["security"]["id"],
                    name = output["security"]["name"],
                    code = output["security"]["code"],
                    currency =  output["security"]["currency"],
                    ticker = output["security"]["ticker"],
                    composite_ticker = output["security"]["composite_ticker"],
                    figi = output["security"]["figi"],
                    composite_figi = output["security"]["composite_figi"],
                    share_class_figi = output["security"]["share_class_figi"],
                    next_page = output["next_page"]
                )

                db.session.merge(api_Response_security_Stock_prices)
                
                added_counter = added_counter + 1
                
            db.session.commit()

            payload["data"] = output

            payload["stats"] = {"total_elements":added_counter}
                
            rootLogger.info("ENDING T: {}".format(output["security"]["composite_ticker"]))

            return payload
                

        except Exception as e:

            rootLogger.info("Exception when calling StockSecurityPrices->bulk_save: %s\n" % e)

            payload = {"result":False}

            payload["reason"] = "API-Exception, please check system logs"

            time.sleep(sleep_time)

            return payload

    def request(self):

        response_time = Timer()

        payload = {"result":True}

        response = []

        global ticker

        ticker = None

        while True:

            try:

                company =  self.get_next_company()

                ticker = company.ticker

                rootLogger.info("COMPANY: {}".format(company.ticker))

                company.progress_1 = True

                db.session.commit()

                bulk_response = self.bulk_save(ticker=company.ticker)

                response.append(bulk_response)

                rootLogger.info("RESPONSE: {}".format(bulk_response))

                company.complete_1 = True
            
                db.session.commit()
                

            except Exception as e:

                rootLogger.info("Exception when calling SecurityPricesBulk->SecurityStocksPricesBulkRS: %s\n" % e)

                payload = {"result":False}

                payload["reason"] = "API-Exception, please check system logs"

                rootLogger.info("SAVED STATUS ERROR!")

                time.sleep(sleep_time)

                db.session.rollback()


                company = ApiResponseCompaniesList.query.filter_by(ticker=ticker).first()
                
                company.error_1 = True
                
                db.session.commit()
                
        return payload
    

############################################ 12TH API Security Adjustments Bulk Version  ###############################################


class SecurityStocksPricesAdjustmentsBulkRS():


    def get_next_company(self):
        
        list_companies = ApiResponseCompaniesList.query.filter_by(complete_2=None, error_2=None, progress_2=None).all()

        if not list_companies:

            rootLogger.info("No companies found")
            
            return []

        return list_companies[0]

    def bulk_save(self, ticker):

        try: 
            
            url = "https://api-v2.intrinio.com/securities/{}/prices/adjustments"

            querystring = {"api_key":API_KEY, "identifier":ticker, "bulk":True}

            payload_request = ""
            headers = {
                'cache-control': "no-cache",
                'Postman-Token': "f2738443-9718-45d6-bd4b-b9e37c8e7d90"
            }

            response = requests.request("GET", url.format(ticker), data=payload_request, headers=headers, params=querystring)

            payload = {"result":True}

            string_data = "["+response.text.split("[")[1].split("]")[0]+"]"

            bulk_data = json.loads(literal_eval("'%s'" % string_data))

            intrinio_sdk.ApiClient().configuration.api_key['api_key'] = API_KEY
            
            rootLogger.info("API SDK intrinio!")

            security_api = intrinio_sdk.SecurityApi()

            api_response = security_api.get_security_stock_price_adjustments(identifier=ticker)

            prepared_output = json.dumps(api_response.to_dict(), default = json_datetime_date_convertor)
            
            output = json.loads(prepared_output)

            added_counter = 0
            for index, item in enumerate(bulk_data):

                preview = ApiResponseSecurityStockPricesAdjustments.query.filter_by(code=output["security"]["code"], composite_ticker = output["security"]["composite_ticker"], date=item["date"]).all()
                if preview:
                    rootLogger.info("{}/{} DUPLICATED: {}".format(index, len(bulk_data),preview))
                    continue

                rootLogger.info("{}/{} TICKER: {} IN PERIOD: {}".format(index, len(bulk_data), output["security"]["composite_ticker"], item["date"]))

                api_Response_security_Stock_prices =  ApiResponseSecurityStockPricesAdjustments(

                    date = item["date"], 
                    dividend_currency = item["dividend_currency"],
                    split_ratio = item["split_ratio"],
                    factor = item["factor"],
                    dividend = item["dividend"],    

                    company_id = output["security"]["company_id"],
                    identifier = output["security"]["id"],
                    security_id = output["security"]["id"],
                    name = output["security"]["name"],
                    code = output["security"]["code"],
                    currency =  output["security"]["currency"],
                    ticker = output["security"]["ticker"],
                    composite_ticker = output["security"]["composite_ticker"],
                    figi = output["security"]["figi"],
                    composite_figi = output["security"]["composite_figi"],
                    share_class_figi = output["security"]["share_class_figi"],
                    next_page = output["next_page"]
                )

                db.session.merge(api_Response_security_Stock_prices)
                
                added_counter = added_counter + 1
                
            db.session.commit()

            payload["data"] = output

            payload["stats"] = {"total_elements":added_counter}
                
            rootLogger.info("ENDING T: {}".format(output["security"]["composite_ticker"]))

            return payload
                

        except Exception as e:

            rootLogger.info("Exception when calling StockSecurityPricesAdjustments->bulk_save: %s\n" % e)

            payload = {"result":False}

            payload["reason"] = "API-Exception, please check system logs"

            time.sleep(sleep_time)

            return payload

    def request(self):

        response_time = Timer()

        payload = {"result":True}

        response = []

        global ticker

        ticker = None

        while True:

            try:

                company =  self.get_next_company()

                ticker = company.ticker

                rootLogger.info("COMPANY: {}".format(company.ticker))

                company.progress_2 = True

                db.session.commit()

                bulk_response = self.bulk_save(ticker=company.ticker)

                response.append(bulk_response)

                rootLogger.info("RESPONSE: {}".format(bulk_response))

                company.complete_2 = True
            
                db.session.commit()
                

            except Exception as e:
                

                rootLogger.info("Exception when calling SecurityPricesAdjustmentsBulk->SecurityStocksPricesAdjustmentsBulkRS: %s\n" % e)

                payload = {"result":False}

                payload["reason"] = "API-Exception, please check system logs"

                rootLogger.info("SAVED STATUS ERROR!")

                time.sleep(sleep_time)

                db.session.rollback()

                company = ApiResponseCompaniesList.query.filter_by(ticker=ticker).first()
                
                company.error_2 = True
                
                db.session.commit()

                
        return payload



############################################ 13TH API Insider OwnerShip Version  ###############################################


class InsiderOwnerShipRS():


    def get_next_company(self):
        
        list_companies = ApiResponseCompaniesList.query.filter_by(complete_3=None, error_3=None, progress_3=None).all()

        if not list_companies:

            rootLogger.info("No companies found")
            
            return []

        return list_companies[0]



    def iterate_pages(self, identifier, n_page=1, data=[]):
        
        url = "https://api.intrinio.com/companies/insider_ownership"

        querystring = {"api_key":"OWZlYzUyNzYwODg1MWNhNjZmZTZjZmE5ZjgxMTZmMjg6YTVhOTRhNjM5MmE2OWJmOTY3NGUzOTJiMjlmZmQwNzk=", "page_number":n_page, "identifier":identifier}

        payload_url = ""

        headers = {
            'cache-control': "no-cache",
            'Postman-Token': "f2738443-9718-45d6-bd4b-b9e37c8e7d90"
            }
        response = requests.request("GET", url, data=payload_url, headers=headers, params=querystring)

        output = json.loads(response.text)

        result_count = output["result_count"]
        current_page = output["current_page"]
        total_pages = output["total_pages"]
        page_size = output["page_size"]
        api_call_credits = output["api_call_credits"]

        rootLogger.info("PAGE {}/{} - {}".format(current_page, total_pages, identifier))
        
        if current_page < total_pages or current_page == 1:

            rootLogger.info(output)

            output["identifier"] = identifier

            current_page = current_page + 1

            rootLogger.info(output)

            data.append(self.bulk_save(output))
            
            self.iterate_pages(identifier, n_page=current_page, data=data)
        
        elif current_page == total_pages:
            
            return data
        
        else:
            
            return data

    def bulk_save(self, bulk_data=[]):
            
        payload = {"result":True}

        added_counter = 0
        
        for index, item in enumerate(bulk_data["data"]):

            preview = ApiResponseInsiderOwnerShip.query.filter_by(owner_cik=item["owner_cik"], owner_name=item["owner_name"], last_reported_date=item["last_reported_date"], value=item["value"], amount=item["amount"]).all()
            if preview:
                rootLogger.info("{}/{} DUPLICATED: {}".format(index, len(bulk_data),preview))
                continue

            rootLogger.info("{}/{} TICKER: {} IN PERIOD: {}".format(index, len(bulk_data["data"]), bulk_data["identifier"], item["last_reported_date"]))

            api_response_us_insider_ownership =  ApiResponseInsiderOwnerShip(
                
                owner_cik = item["owner_cik"],
                owner_name = item["owner_name"],
                last_reported_date = item["last_reported_date"],
                value = item["value"],
                amount = item["amount"],
                result_count = bulk_data["result_count"],
                page_size = bulk_data["page_size"],
                current_page = bulk_data["current_page"],
                total_pages = bulk_data["total_pages"],
                api_call_credits = bulk_data["api_call_credits"],
                ticker = bulk_data["identifier"]
            )

            db.session.merge(api_response_us_insider_ownership)

            added_counter = added_counter + 1

        db.session.commit()

        payload["data"] = bulk_data

        payload["stats"] = {"total_elements":added_counter}

        rootLogger.info("ENDING T: {}".format(bulk_data["identifier"]))

        return payload



    def request(self):

        response_time = Timer()

        payload = {"result":True}

        response = []

        global ticker

        ticker = None

        while True:

            try:

                company =  self.get_next_company()

                ticker = company.ticker

                rootLogger.info("COMPANY: {}".format(company.ticker))

                company.progress_3 = True

                db.session.commit()

                bulk_response = self.iterate_pages(identifier=company.ticker)

                response.append(bulk_response)

                rootLogger.info("RESPONSE: {}".format(bulk_response))

                company.complete_3 = True
            
                db.session.commit()
                

            except Exception as e:
                

                rootLogger.info("Exception when calling InsiderOwnerShipRS->InsiderOwnerShipRS: %s\n" % e)

                payload = {"result":False}

                payload["reason"] = "API-Exception, please check system logs"

                rootLogger.info("SAVED STATUS ERROR!")

                time.sleep(sleep_time)

                db.session.rollback()

                company = ApiResponseCompaniesList.query.filter_by(ticker=ticker).first()
                
                company.error_3 = True
                
                db.session.commit()

                
        return payload



############################################ 14TH API Owners Version  ###############################################


class OwnersRS():


    def get_next_company(self):
        
        list_companies = ApiResponseCompaniesList.query.filter_by(complete_4=None, error_4=None, progress_4=None).all()

        if not list_companies:

            rootLogger.info("No companies found")
            
            return []

        return list_companies[0]



    def iterate_pages(self, identifier, n_page=1, data=[]):
        
        url = "https://api.intrinio.com/owners"

        querystring = {"api_key":"OWZlYzUyNzYwODg1MWNhNjZmZTZjZmE5ZjgxMTZmMjg6YTVhOTRhNjM5MmE2OWJmOTY3NGUzOTJiMjlmZmQwNzk=", "page_number":n_page, "identifier":identifier, "bulk":True}

        payload_url = ""

        headers = {
            'cache-control': "no-cache",
            'Postman-Token': "f2738443-9718-45d6-bd4b-b9e37c8e7d90"
            }
        response = requests.request("GET", url, data=payload_url, headers=headers, params=querystring)

        output = json.loads(response.text)

        print(output)

        result_count = output["result_count"]
        current_page = output["current_page"]
        total_pages = output["total_pages"]
        page_size = output["page_size"]
        api_call_credits = output["api_call_credits"]

        rootLogger.info("PAGE {}/{} - {}".format(current_page, total_pages, identifier))
        
        if current_page < total_pages or current_page == 1:

            rootLogger.info(output)

            output["identifier"] = identifier

            current_page = current_page + 1

            rootLogger.info(output)

            data.append(self.bulk_save(output))
            
            self.iterate_pages(identifier, n_page=current_page, data=data)
        
        elif current_page == total_pages:
            
            return data
        
        else:
            
            return data

    def bulk_save(self, bulk_data=[]):
    
        print(bulk_data)

        payload = {"result":True}

        added_counter = 0
        
        for index, item in enumerate(bulk_data["data"]):

            preview = ApiResponseOwners.query.filter_by(owner_cik=item["owner_cik"], owner_name=item["owner_name"], ticker=bulk_data["identifier"]).all()
            if preview:
                rootLogger.info("{}/{} DUPLICATED: {}".format(index, len(bulk_data),preview))
                continue

            rootLogger.info("{}/{} TICKER: {}".format(index, len(bulk_data["data"]), bulk_data["identifier"]))

            api_response_owners =  ApiResponseOwners(
                
                owner_cik = item["owner_cik"],
                owner_name = item["owner_name"],
                result_count = bulk_data["result_count"],
                page_size = bulk_data["page_size"],
                current_page = bulk_data["current_page"],
                total_pages = bulk_data["total_pages"],
                api_call_credits = bulk_data["api_call_credits"],
                ticker = bulk_data["identifier"]
            )

            db.session.merge(api_response_owners)

            added_counter = added_counter + 1

        db.session.commit()

        payload["data"] = bulk_data

        payload["stats"] = {"total_elements":added_counter}

        rootLogger.info("ENDING T: {}".format(bulk_data["identifier"]))

        return payload



    def request(self):

        response_time = Timer()

        payload = {"result":True}

        response = []

        global ticker

        ticker = None

        while True:

            try:

                company =  self.get_next_company()

                ticker = company.ticker

                rootLogger.info("COMPANY: {}".format(company.ticker))

                company.progress_4 = True

                db.session.commit()

                bulk_response = self.iterate_pages(identifier=company.ticker)

                response.append(bulk_response)

                rootLogger.info("RESPONSE: {}".format(bulk_response))

                company.complete_4 = True
            
                db.session.commit()
                

            except Exception as e:
                

                rootLogger.info("Exception when calling OwnersRS->OwnersRS: %s\n" % e)

                payload = {"result":False}

                payload["reason"] = "API-Exception, please check system logs"

                rootLogger.info("SAVED STATUS ERROR!")

                time.sleep(sleep_time)

                db.session.rollback()

                company = ApiResponseCompaniesList.query.filter_by(ticker=ticker).first()
                
                company.error_4 = True
                
                db.session.commit()

                
        return payload

    
############################################ 15TH API insider_transactions Version  ###############################################


class InsiderTransactionsRS():


    def get_next_company(self):
        
        list_companies = ApiResponseCompaniesList.query.filter_by(complete_5=None, error_5=None, progress_5=None).all()

        if not list_companies:

            rootLogger.info("No companies found")
            
            return []

        return list_companies[0]



    def iterate_pages(self, identifier, n_page=1, data=[]):
        
        url = "https://api.intrinio.com/companies/insider_transactions"

        querystring = {"api_key":"OWZlYzUyNzYwODg1MWNhNjZmZTZjZmE5ZjgxMTZmMjg6YTVhOTRhNjM5MmE2OWJmOTY3NGUzOTJiMjlmZmQwNzk=", "page_number":n_page, "identifier":identifier, "bulk":True}

        payload_url = ""

        headers = {
            'cache-control': "no-cache",
            'Postman-Token': "f2738443-9718-45d6-bd4b-b9e37c8e7d90"
            }
        response = requests.request("GET", url, data=payload_url, headers=headers, params=querystring)

        output = json.loads(response.text)

        print(output)

        result_count = output["result_count"]
        current_page = output["current_page"]
        total_pages = output["total_pages"]
        page_size = output["page_size"]
        api_call_credits = output["api_call_credits"]

        rootLogger.info("PAGE {}/{} - {}".format(current_page, total_pages, identifier))
        
        if current_page < total_pages or current_page == 1:

            rootLogger.info(output)

            output["identifier"] = identifier

            current_page = current_page + 1

            rootLogger.info(output)

            data.append(self.bulk_save(output))
            
            self.iterate_pages(identifier, n_page=current_page, data=data)
        
        elif current_page == total_pages:
            
            return data
        
        else:
            
            return data

    def bulk_save(self, bulk_data=[]):
    
        print(bulk_data)

        payload = {"result":True}

        added_counter = 0
        
        for index, item in enumerate(bulk_data["data"]):

            preview = ApiResponseInsiderTransactions.query.filter_by(owner_cik=item["owner_cik"], owner_name=item["owner_name"], ticker=bulk_data["identifier"], transaction_date=item["transaction_date"], filing_date=item["filing_date"], filing_url=item["filing_url"], amount_of_shares=item["amount_of_shares"], deemed_execution_date=item["deemed_execution_date"]).all()
            if preview:
                rootLogger.info("{}/{} DUPLICATED: {}".format(index, len(bulk_data),preview))
                continue

            rootLogger.info("{}/{} TICKER: {}".format(index, len(bulk_data["data"]), bulk_data["identifier"]))

            api_response_insider_transactions =  ApiResponseInsiderTransactions(
                
                owner_cik = item["owner_cik"],
                owner_name = item["owner_name"],
                result_count = bulk_data["result_count"],
                page_size = bulk_data["page_size"],
                current_page = bulk_data["current_page"],
                total_pages = bulk_data["total_pages"],
                api_call_credits = bulk_data["api_call_credits"],
                ticker = bulk_data["identifier"],

                filing_date = item.get("filing_date", None),
                filing_url = item.get("filing_url", None),
                director = item.get("director", None),
                officer = item.get("officer", None),
                ten_percent_owner = item.get("ten_percent_owner", None),
                other_relation = item.get("other_relation", None),
                officer_title = item.get("officer_title", None),
                derivative_transaction = item.get("derivative_transaction", None),
                security_title = item.get("security_title", None),
                conversion_exercise_price = item.get("conversion_exercise_price", None),
                transaction_date = item.get("transaction_date", None),
                deemed_execution_date = item.get("deemed_execution_date", None),
                transaction_type_code = item.get("transaction_type_code", None),
                acquisition_disposition_code = item.get("acquisition_disposition_code", None),
                amount_of_shares = item.get("amount_of_shares", None),
                exercise_date= item.get("exercise_date", None),
                expiration_date = item.get("expiration_date", None),
                underlying_security_title = item.get("underlying_security_title", None),
                underlying_shares = item.get("underlying_shares", None),
                transaction_price = item.get("transaction_price", None),
                total_shares_owned = item.get("total_shares_owned", None),
                ownership_type_code = item.get("ownership_type_code", None),
                report_line_number = item.get("report_line_number", None)
            )

            db.session.merge(api_response_insider_transactions)

            added_counter = added_counter + 1

        db.session.commit()

        payload["data"] = bulk_data

        payload["stats"] = {"total_elements":added_counter}

        rootLogger.info("ENDING T: {}".format(bulk_data["identifier"]))

        return payload



    def request(self):

        response_time = Timer()

        payload = {"result":True}

        response = []

        global ticker

        ticker = None

        while True:

            try:

                company =  self.get_next_company()

                ticker = company.ticker

                rootLogger.info("COMPANY: {}".format(company.ticker))

                company.progress_5 = True

                db.session.commit()

                bulk_response = self.iterate_pages(identifier=company.ticker)

                response.append(bulk_response)

                rootLogger.info("RESPONSE: {}".format(bulk_response))

                company.complete_5 = True
            
                db.session.commit()
                

            except Exception as e:
                

                rootLogger.info("Exception when calling InsiderTransactionsRS->InsiderTransactionsRS: %s\n" % e)

                payload = {"result":False}

                payload["reason"] = "API-Exception, please check system logs"

                rootLogger.info("SAVED STATUS ERROR!")

                time.sleep(sleep_time)

                db.session.rollback()

                company = ApiResponseCompaniesList.query.filter_by(ticker=ticker).first()
                
                company.error_5 = True
                
                db.session.commit()

                
        return payload