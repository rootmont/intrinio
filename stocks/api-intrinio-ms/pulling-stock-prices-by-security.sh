cd ${HOME}/stocks/api-intrinio-ms
echo "Starting stock-prices-by-security thread"
source venv/bin/activate
export FLASK_APP=flask_app.py
export env=database
python3.6 -m flask stock-prices-by-security
