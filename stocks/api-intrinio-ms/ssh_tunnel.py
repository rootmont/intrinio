from sshtunnel import SSHTunnelForwarder
from time import sleep

with SSHTunnelForwarder(
    ('ec2-18-220-96-3.us-east-2.compute.amazonaws.com', 22),
    ssh_username="anthony",
    ssh_password="hianthony",
    remote_bind_address=('stockapi.cluster-cshpiwdqubcf.us-east-2.rds.amazonaws.com', 3306),
    local_bind_address=('0.0.0.0', 3306),
    ssh_private_key='/Users/anthonymunoz/Documents/stocks/api-intrinio-ms/anthony-robert.pem'

) as server:

    print(server.local_bind_port)
    while True:
        # press Ctrl-C for stopping
        sleep(1)
        
print('FINISH!')